import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Button,
  SafeAreaView,
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
// import {Picker} from '@react-native-picker/picker';
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { vehicle_list } from '../../redux/actions/vehicle_list';
import { setSpeed } from '../../redux/actions/setSpeed';
class SpeedLimit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',
      pickedVehicle: '',
      vehicle_list: [],
      speed: 100
    }
    this._getStorageValue()
  }
  data = [
    { label: "Select Vehicle ", value: 1 },
  ];
  handlePicker = data => {
    this.setState({ pickedVehicle: data });
    console.log(data)
    this.setState({ vehicle_id: data.value })
  };
  submitSpeed() {
    var vehicle = this.state.pickedVehicle.value;
    var speed = this.state.speed
    this.props.setSpeed(this.state).then(response => {
      // console.log(this.props.setSpeed1.message)
      // add alert here 
      this.props.navigation.navigate('Home')
    }
    )
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })
      this.props.vehicle_list(value).then(() => {
        var vehicle_list = []
        vehicle_list = this.props.vehicle_list1.map((obj) => {
          var obj1 = {
            label: obj.vehicle_no,
            value: obj.vehicle_id
          }
          return obj1
          // console.log(obj1)
        })
        this.data = vehicle_list
        this.setState({ vehicle_list: this.props.vehicle_list1 })
      })
    }
    return value
  }
  render() {
    console.log(this.props.vehicle_list1)
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Speed Limit</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.pickerBox}>
              <Image source={Images.car_white} style={styles.carIcon} />
              <Picker
                onItemChange={this.handlePicker}
                style={styles.picker}
                items={this.data}
                placeholder="All Vehicles"
                placeholderStyle={{ color: '#fff' }}
                item={this.state.pickedVehicle}
              // backdropAnimation={{ opactity: 0 }}
              //androidPickerMode="dropdown"
              //isNullable
              //disable
              />
              <Image source={Images.white_dropdown} style={styles.dropdown} />
            </View>
            <View style={styles.formBox}>
              <TextInput style={styles.formshortInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={(speed) => { this.setState({ speed }) }}
                placeholder="Enter Speed Limit" />
            </View>
            <TouchableOpacity style={styles.submit} onPress={() => this.submitSpeed()}>
              <Text style={styles.submitText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    vehicle_list1: state.vehicle_list.vehicle_list,
    setSpeed1: state.setSpeed.setSpeed
  };
};
export default connect(mapStateToProps, {
  vehicle_list, setSpeed
})(SpeedLimit);
