import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,
    FlatList,
    Button,
    SafeAreaView,
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
const { width, height } = Dimensions.get('window');
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMapMarker, faClock, faPlay, faStop } from '@fortawesome/free-solid-svg-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux'
import { trip_report } from '../../redux/actions/trip_report'
import MapView, { MarkerAnimated, PROVIDER_GOOGLE, Marker, Polyline } from 'react-native-maps';
class TripInformation extends React.Component {
    constructor(props) {
        super(props);
        var date = this.props.route.params.from_date;
        date = this.fixDate(date)
        this.state = {
            loading: true,
            vehicle_id: this.props.route.params.vehicle_id,
            from_date: date,
            trip_data: []
        }
        this._getStorageValue()
    }
    fixDate(date) {
        var newdate = date.split('-')
        return newdate[2] + '-' + newdate[1] + '-' + newdate[0]
    }
    async _getStorageValue() {
        var value = await AsyncStorage.getItem('token')
        if (value != null) {
            this.setState({ token: value })
            this.props.trip_report(this.state).then(() => {
                console.log(this.props.trip_report1, 'shashank data')
                this.setState({ trip_data: this.props.trip_report1, loading: false })
                console.log(this.state.trip_data)
            })
        }
        return value
    }
    renderMarker(item) {
        var coordinates = JSON.parse(item.trip_coordinate)
        console.log(coordinates.length, 'shashankshekahr')
        if (coordinates.length < 1) {
            var herodata = <></>
        }
        else {
            var herodata = coordinates.map((element, index) => {
                var coords = element.split(',');
                if (typeof Number(coords[0]) == 'number') {
                    // console.log(Number(coords[0]), Number(coords[1]), 'shashank')
                    return < MapView.Marker
                        coordinate={{
                            latitude: Number(coords[0]) ? Number(coords[0]) : 0,
                            longitude: Number(coords[1]) ? Number(coords[1]) : 0,
                        }
                        }
                        pinColor={'red'}
                        title={'position ' + index}
                    />
                }
                else
                    return;
            })
            return <>{herodata}</>
        }
    }
    renderPolyline(item) {
        var coordinates = JSON.parse(item.trip_coordinate)
        if (coordinates.length < 1) {
            var herodata = <></>
        }
        else {
            var herodata = coordinates.map((element, index) => {
                var coords = element.split(',');
                if (typeof Number(coords[0]) == 'number') {
                    var ltlng = {
                        latitude: Number(coords[0]) ? Number(coords[0]) : 0,
                        longitude: Number(coords[1]) ? Number(coords[1]) : 0
                    }
                    return ltlng
                }
                else
                    return;
            })
            return < Polyline
                key={item.id}
                coordinates={herodata}
                strokeColor="#000"
                fillColor="rgba(255,0,0,0.5)"
                strokeWidth={1}
            />
        }
    }
    render() {
        var mapStyle = [
            { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#F9E9D0" }] },
        ];
        if (this.state.loading) {
            return <SafeAreaView style={styles.container}>
                <View style={styles.wrapper}>
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home"); }}>
                                <Image source={Images.back} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>Trip Information</Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: height * 0.35 }}>
                        <Image source={Images.logo_new2} style={{ width: width * 0.2, height: width * 0.2 }} />
                        <Text style={{ fontSize: 16, marginTop: 20 }}>Loading Data...</Text>
                        <Text style={{ fontSize: 16, marginTop: 20 }}> Please wait for 5 minutes...</Text>
                    </View>
                </View>
            </SafeAreaView>
        }
        else
            return (
                <SafeAreaView style={styles.container}>
                    <View style={styles.wrapper}>
                        <View style={styles.upperBar}>
                            <View style={styles.backIconContainer}>
                                <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home"); }}>
                                    <Image source={Images.back} style={styles.barMenuIcon} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headingContainer}>
                                <Text style={styles.barText}>Trip Information</Text>
                            </View>
                        </View>
                        <KeyboardAvoidingView behavior="padding">
                            {/* <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}> */}
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{ paddingBottom: '55%', }}
                                data={this.state.trip_data}
                                // data={this.props.trip_report1}
                                renderItem={({ item, index }) => (
                                    <>
                                        <View style={{ elevation: 5, width: '100%', height: height * 0.75, backgroundColor: '#fff', borderBottomColor: '#fff', borderBottomWidth: 2 }}>
                                            <View>
                                                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                                    {/* <Image source={Images.refresh_blue} style={styles.barMenuIcon} /> */}
                                                    <Text style={{ marginLeft: 20, fontSize: 18 }}><Text style={{ fontWeight: 'bold' }}>Date :</Text> {item.report_date}</Text>
                                                </View>
                                                <View style={{ marginTop: '5%' }}>
                                                    <Text style={{ marginLeft: 20 }}><Text style={{ fontWeight: 'bold' }}>Trip No. :</Text> {index + 1}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    {/* <Text style={{ marginLeft: 20 }}><Text style={{ fontWeight: 'bold' }}>Trip Start Time :</Text> 2/2/20121</Text> */}
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginLeft: 20, marginTop: 10 }}>
                                                        <FontAwesomeIcon icon={faPlay} color={'green'} />
                                                        <Text style={{ marginLeft: 5, fontWeight: 'bold' }}>Trip Start :</Text>
                                                        <Text style={{ marginLeft: 10 }}>{item.trip_start.split(' ')[1]}</Text>
                                                    </View>
                                                    <Text style={{ marginLeft: 10, marginTop: 10 }}><Text style={{ fontWeight: 'bold' }}>Total Distance :</Text> {Math.round(item.distance / 1000)} KM</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginLeft: 20, marginRight: 20 }}>
                                                    {/* <Text style={{ marginLeft: 10 }}><Text style={{ fontWeight: 'bold' }}>Location :</Text> Gurudwara Road,Panipat</Text> */}
                                                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                        <FontAwesomeIcon icon={faMapMarker} color={'green'} />
                                                        <Text style={{ marginLeft: 10 }}>{item.start}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles.formWrapper}>
                                                    <MapView
                                                        style={{ flex: 1, width: '100%', height: '40%' }}
                                                        region={{
                                                            latitude: parseFloat(item.trip_start_lat),
                                                            longitude: parseFloat(item.trip_start_long),
                                                            latitudeDelta: 0.0922,
                                                            longitudeDelta: 0.0421
                                                        }}
                                                        paddingAdjustmentBehavior={'always'}
                                                        customMapStyle={mapStyle}
                                                        showsUserLocation
                                                    >
                                                        {this.renderMarker(item)}
                                                        {this.renderPolyline(item)}
                                                    </MapView>
                                                </View>
                                            </View>
                                            {item.stops = JSON.parse(item.trip_stops)}
                                            {/* <View style={{borderWidth:2,backgroundColor:'#fff'}}> */}
                                            <FlatList
                                                showsVerticalScrollIndicator={false}
                                                // horizontal={true}
                                                data={item.stops}
                                                renderItem={({ item2 }) => (
                                                    <>
                                                        {console.log(item)}
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10, marginLeft: 20, marginRight: 20 }}>
                                                            {/* <Text style={{ marginLeft: 10 }}><Text style={{ fontWeight: 'bold' }}>Location :</Text> Gurudwara Road,Panipat</Text> */}
                                                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                                <FontAwesomeIcon icon={faMapMarker} color={'red'} />
                                                                <Text style={{ marginLeft: 10 }}>{item2.latitude + ',' + item2.longitude}</Text>
                                                            </View>
                                                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                                <FontAwesomeIcon icon={faClock} color={'black'} />
                                                                <Text style={{ marginLeft: 10 }}>1 min</Text>
                                                            </View>
                                                        </View>
                                                    </>
                                                )}
                                                keyExtractor={item2 => item2.id}
                                            />
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 30, marginLeft: 20, marginRight: 20 }}>
                                                {/* <Text style={{ marginLeft: 10 }}><Text style={{ fontWeight: 'bold' }}>Location :</Text> Gurudwara Road,Panipat</Text> */}
                                                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                                    <FontAwesomeIcon icon={faStop} color={'red'} />
                                                    <Text style={{ marginLeft: 10, fontWeight: 'bold' }}>Trip End</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                    <FontAwesomeIcon icon={faClock} color={'black'} />
                                                    <Text style={{ marginLeft: 10 }}>{item.trip_end.split(' ')[1]}</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, marginLeft: 20, marginRight: 20 }}>
                                                {/* <Text style={{ marginLeft: 10 }}><Text style={{ fontWeight: 'bold' }}>Location :</Text> Gurudwara Road,Panipat</Text> */}
                                                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                                    <FontAwesomeIcon icon={faMapMarker} color={'green'} />
                                                    <Text style={{ marginLeft: 10 }}>{item.end}</Text>
                                                </View>
                                            </View>
                                        </View>
                                        {/* </View> */}
                                    </>
                                )}
                                keyExtractor={item => item.id}
                            />
                            {/* </ScrollView> */}
                        </KeyboardAvoidingView>
                    </View>
                </SafeAreaView>
            );
    }
}
const mapStateToProps = (state) => {
    return {
        trip_report1: state.trip_report.trip_report
    };
};
export default connect(mapStateToProps, {
    trip_report
})(TripInformation);
