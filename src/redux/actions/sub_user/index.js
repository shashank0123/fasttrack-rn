import axios from 'axios';
import urlObj from '../../url';
export const sub_user = (token) => {
  return async (dispatch, getState) => {

    url = urlObj.sub_user_url + "?token=" + token;
    console.log(url)
    await axios
      .get(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'sub_user',
          payload: response.data.users

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
export const sub_user_status = (token, user_id, status) => {
  return async (dispatch, getState) => {

    url = urlObj.sub_user_status_url + "?token=" + token + "&status=" + status + "&user_id=" + user_id;
    console.log(url)
    await axios
      .post(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'STATUS',
          payload: response.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};