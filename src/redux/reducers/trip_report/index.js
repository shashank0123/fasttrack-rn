const initialState = {
  trip_report: {}
};

export const trip_report = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN': {
      // console.log(action.payload)
      return { ...state, trip_report: action.payload };
    }
    default: {
      return state;
    }
  }
};
export default trip_report;