const initialState = {
    delete_id: []
  };
  
  export const delete_id = (state = initialState, action) => {
    switch (action.type) {
      case 'delete_id': {
        // console.log(action.payload)
        return { ...state,delete_id:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default delete_id;