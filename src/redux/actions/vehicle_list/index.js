import axios from 'axios';
import urlObj from '../../url';
export const vehicle_list = (token) => {
  return async (dispatch, getState) => {

    url = urlObj.vehicle_list_url + "?token=" + token;
    await axios
      .get(url)
      .then((response) => {
        // console.log(response.data)
        dispatch({
          type: 'vehicle_list',
          payload: response.data.vehicles

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};