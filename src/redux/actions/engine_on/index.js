import axios from 'axios';
import urlObj from '../../url';
export const engine_on = (data) => {
  return async (dispatch, getState) => {

    url = urlObj.engine_on_url + "?token=" + data.token + "&vehicle_id=" + data.vehicle_id;
    console.log(url)
    await axios
      .post(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'LOGIN',
          payload: response.data.message

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};