import axios from 'axios';
import urlObj from '../../url';
export const trip_report = (state) => {
  return async (dispatch, getState) => {

    url = urlObj.trip_report_url + "?token=" + state.token + "&vehicle_id=" + state.vehicle_id + "&from_date=" + state.from_date;
    console.log(url)
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'LOGIN',
          payload: response.data.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};