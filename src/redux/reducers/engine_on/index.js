const initialState = {
  engine_on: {}
  };
  
  export const engine_on = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        // console.log(action.payload)
        return { ...state,engine_on:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default engine_on;