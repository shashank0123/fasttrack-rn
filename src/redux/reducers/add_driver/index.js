const initialState = {
    adddriver: {}
  };
  
  export const adddriver = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        // console.log(action.payload)
        return { ...state,adddriver:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default adddriver;