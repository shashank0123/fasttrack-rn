import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        position: 'relative',
        backgroundColor: '#fff'
    },
    wrapper: {
        width,
        height,
        display: 'flex',
        justifyContent: 'flex-start',
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor: '#000000',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '60%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#ffffff",
        fontSize: height * 0.03,
    },
    scrollWrapper: {
        width,
        display: 'flex',
        justifyContent: 'flex-start',
        // alignItems: 'center',
        paddingBottom: 70
        // backgroundColor: 'pink',
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    formWrapper: {
        height: height * 0.3,
        marginTop: height * 0.02,
        width,
        alignItems: "center",
        // borderRadius: width * 0.02,
        // marginLeft: 30,
        // borderLeftWidth: 2
        // borderWidth: 2,
    },
    map: {
        width: width * 0.9,
        height: height * 0.3,
        borderWidth: 1, borderColor: '#000'
    }
});
