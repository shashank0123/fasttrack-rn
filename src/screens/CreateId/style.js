import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        // flex:1
    
        // position: 'relative',
    },
    scrollWrapper: {
        width,
        display: 'flex',
        justifyContent: 'flex-start',
        // alignItems: 'center',
        paddingBottom: 35
        // backgroundColor: 'pink',
    },
    wrapper: {
        width,
        // height,
        display: 'flex',
        // justifyContent: 'flex-start',
    },
    upperBar: {
        width,
        height: height*0.075,
        backgroundColor: '#000000',
        flexDirection:'row',
        alignItems:'center',
    },
    bottomBar: {
        width,
        height: height*0.075,
        backgroundColor: '#000000',
        flexDirection:'row',
        justifyContent:"center",
        alignItems:'center',
        position:'absolute',
        bottom:0

        
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '60%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    bottomContainer: {
       
       
        alignItems:'center',
        justifyContent:'center'
    },
    barText: {
       color:"#ffffff",
       fontSize: height*0.03, 
    },
    barMenuIcon: {
        height:height*0.04,
        width:height*0.04,
        marginLeft:width*0.04,
    },
    barNavIcon: {
        height:height*0.05,
        width:height*0.035,
        marginLeft:width*0.27,
    },
    barRefreshIcon: {
        height:height*0.04,
        width:height*0.04,
        marginLeft:width*0.03,
    },
    searchWrapper: {
        height:height*0.075,
        width:width*0.9,
        alignSelf:"center",
        marginTop:height*0.01,
        backgroundColor: "#dedede",
        flexDirection:'row',
        alignItems:'center',
    },
    searchMenuIcon: {
        height:height*0.04,
        width:height*0.04,
        marginLeft:width*0.03,
    },
    searchTextInput:{
        height:'100%',
        width:'70%',
        marginLeft:width*0.03,
        fontSize:width*0.04,
    },
    callIcon: {
        height:height*0.04,
        width:height*0.04,
        marginLeft:width*0.02,
        position:"absolute",
        right:'5%',
    },
    flatlistWrapper: {
        height:height*0.85,
        marginTop:height*0.01,
        width,
        justifyContent:'center',
        alignItems:'center',
        // paddingBottom:90
    },
    card: {
        width:'95%',
        height:'80%',
        alignSelf:'center',
        borderRadius:width*0.01,
        padding:width*0.03,
        alignItems:'center',
        flexDirection:'row',
        elevation:2
    },
    cardHeader: {
        width:'100%',
        height:'18%',
        flexDirection:'row',
        alignItems:"center",
    },
    cardImages: {
        width:'100%',
        height:'65%',
        flexDirection:'row',
    },
    cardImagesWrapper: {
        width:'25%',
        height:'100%',
        flexDirection:'column',
        alignItems:"center",
    },
    cardAddress: {
        borderTopWidth:1,
        width:'100%',
        height:'20%',
    },
    nameWrapper:{
        width:'30%',
        height:'100%',
    },
    slWrapper:{
        width:'45%',
        height:'100%',
        alignItems:'center',
        justifyContent:'center',
    },
    dateWrapper:{
        width:'25%',
        height:'100%',
        justifyContent:'flex-start',
    },
    name: {
        fontSize:width*0.04,
        fontWeight:'bold',
    },
    speed_limit: {
        fontSize:width*0.035,
        fontWeight:'bold',
    },
    date: {
        fontSize:width*0.025,
        fontWeight:'bold',
        alignSelf:'flex-end'
    },
    address: {
        fontSize:width*0.025,
        fontWeight:'bold',
    },
    cardImagesText:{
        fontSize:width*0.025,
    },
    cardCircle: {
        width:width*0.12,
        height:width*0.12,
        backgroundColor: '#e6e6e6',
        borderRadius:width*0.06,
        alignItems:'center',
        justifyContent:'center',
    },
    cardStatusCircleGreen: {
        width:width*0.12,
        height:width*0.12,
        backgroundColor: '#009015',
        borderRadius:width*0.06,
        alignItems:'center',
        justifyContent:'center',
    },
    cardStatusCircleBlue: {
        width:width*0.12,
        height:width*0.12,
        backgroundColor: '#4d4dff',
        borderRadius:width*0.06,
        alignItems:'center',
        justifyContent:'center',
    },
    circleIcons: {
        width:width*0.10,
        height:width*0.10,
    },
    cardWrapper: {
        // height: height*0.09,
        alignItems:"center",
        justifyContent:"center",
    },
    docDetailedWrapper2: {
        width: width * 0.9,
        // height: height * 0.1,
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#ffffff',
        // marginTop: height * 0.02,
        borderRadius: 10,
        shadowColor: 'rgba(1, 1, 1, 1)',
        // elevation: 4,
        // marginLeft: 18,
        borderBottomWidth: .5,
        borderBottomColor: "#C8C8C8",
        marginTop:10,
        paddingBottom:5
    },
    docSpecsWrapper: {
        width: '93%',
        // display: 'flex',
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center'
    },
    docdetailImg: {
        width: 50,
        height: 50,
        borderRadius: 30,
        borderWidth: 1,
        // borderColor: colors.WHITE,
        // shadowColor: colors.BLACK,
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 1,
        elevation: 5,
        marginBottom: 4
    },
    docNameWrapper: {
        width: '80%',
        marginLeft: 15,
        display: 'flex',
        // marginTop: 7,
        flexDirection: 'column',
    },
    docNameText: {
        fontFamily: 'Helvetica Neue',
        color: '#000',
        fontSize: width * 0.038,
        // fontWeight: 'bold',
        paddingBottom: 1,
    },
    docSubNameText: {
        // color: '#898A8F',
        color: '#000',
        fontSize: width * 0.033,
        fontWeight: '400',
        fontFamily: 'Helvetica Neue',
        fontStyle: 'normal',
        marginTop:'2%'
    },
    boxBtn: {
        width: width * 0.2,
        height: height * 0.06,
        display: 'flex',
        backgroundColor: '#000',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        // flexDirection: 'row',
        marginTop: '7%',
        marginRight:15
    },
    bellBox: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '7%'
    },
    btnText: {
        // width:'100%',
        fontWeight: 'bold',
        textShadowColor: '#ffffff',
        fontSize: 12,
        color: '#fff',
        // marginLeft: '8%'
    },

     
});
