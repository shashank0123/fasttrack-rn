import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Button,
  SafeAreaView,
} from "react-native";

import styles from './style';
import { Images } from '../../utils';
// import {Picker} from '@react-native-picker/picker';
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { vehicle_list } from '../../redux/actions/vehicle_list';
import { complaint } from '../../redux/actions/complaint';
class Complaint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',
      pickedVehicle: '',
      vehicle_list: []
    }
    this._getStorageValue()
  }
  data = [
    { label: "Select Vehicle ", value: 1 },
  ];
  handlePicker = data => {
    this.setState({ pickedVehicle: data });
    console.log(data)
    this.setState({ vehicle_no: data.label })
  };
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })
      this.props.vehicle_list(value).then(() => {

        var vehicle_list = []
        vehicle_list = this.props.vehicle_list1.map((obj) => {
          var obj1 = {
            label: obj.vehicle_no,
            value: obj.vehicle_id
          }
          return obj1
          // console.log(obj1)
        })
        this.data = vehicle_list
        this.setState({ vehicle_list: this.props.vehicle_list1 })
      })


    }
    return value
  }


  submitComplaint() {
    this.props.complaint(this.state).then(() => {



      this.setState({ vehicle_list: this.props.complaint1 })
      this.props.navigation.navigate('Home')
    })
  }
  render() {
    console.log(this.props.vehicle_list1)
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Complaint</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.pickerBox}>
              <Image source={Images.car_white} style={styles.carIcon} />
              {/* <Picker
                selectedValue={this.state.selectedValue}
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) => this.setState({selectedValue: itemValue})}>
                  <Picker.Item label = "All Vehicles" value = "All Vehicles" />
                {vehicles.map(vehc => <Picker.Item key={vehc.id} label={vehc.name} value={vehc.name} />)}
              </Picker> */}
              <Picker
                onItemChange={this.handlePicker}
                style={styles.picker}
                items={this.data}
                placeholder="All Vehicles"
                placeholderStyle={{ color: '#fff' }}
                item={this.state.pickedVehicle}
              // backdropAnimation={{ opactity: 0 }}
              //androidPickerMode="dropdown"
              //isNullable
              //disable
              />
              <Image source={Images.white_dropdown} style={styles.dropdown} />
            </View>
            <View style={styles.formBox}>
              <TextInput style={styles.formshortInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={(name) => { this.setState({ name }) }}
                placeholder="Full Name" />
              <Image source={Images.person_Icon} style={styles.searchIcon} />
            </View>
            <View style={styles.formBox}>
              <TextInput style={styles.formshortInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={(mobile) => { this.setState({ mobile }) }}
                placeholder="Mobile Number" />
              <Image source={Images.call_Icon} style={styles.callIcon} />
            </View>
            <View style={styles.formBigBox}>
              <TextInput style={styles.formlongInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={(complaint) => { this.setState({ complaint }) }}
                placeholder="Complaint"
                multiline={true}
                blurOnSubmit={true}
                textAlignVertical="top" />
              {/* <Image source={Images.search_grey} style={styles.searchIcon}/> */}
            </View>
            <TouchableOpacity style={styles.submit} onPress={() => { this.submitComplaint() }}>
              <Text style={styles.submitText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    vehicle_list1: state.vehicle_list.vehicle_list,
    complaint1: state.complaint.complaint
  };
};
export default connect(mapStateToProps, {
  vehicle_list, complaint
})(Complaint);

