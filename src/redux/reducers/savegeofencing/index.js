const initialState = {
    savegeofencing: []
  };
  
  export const savegeofencing = (state = initialState, action) => {
    switch (action.type) {
      case 'SAVEGEOFENCING': {
        // console.log(action.payload)
        return { ...state,savegeofencing:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default savegeofencing;