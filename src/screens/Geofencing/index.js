import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  PermissionsAndroid,
  Platform,
  SafeAreaView
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
// import {Picker} from '@react-native-picker/picker';
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { vehicle_list } from '../../redux/actions/vehicle_list';
import { geofencing } from '../../redux/actions/geofencing';
import { savegeofencing } from '../../redux/actions/savegeofencing';
import Slider from "react-native-slider";
import axios from 'axios'
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
class Geofencing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',
      pickedVehicle: '',
      vehicle_list: [],
      // distance: 30,
      radius: 30,
      minDistance: 10,
      maxDistance: 150,
      address: '',
      latitude: '',
      longitude: '',
      // radius: '',
      status: '',
      latitude: 0,
      longitude: 0,
      error: null,
    }
    this._getStorageValue()
    Geocoder.init("AIzaSyAahyvaPX2TNgAFrWBuZ8wEWerbgex4YRw");
    this.requestPermissions()
  }
  async getLocation() {
    this.requestPermissions()
    Geolocation.getCurrentPosition(
      async (position) => {
        console.log(position.display_name, 'shashanks')
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
        var url = 'https://us1.locationiq.com/v1/reverse.php?key=pk.98c895978683809e09c5fa1db1ac56ec&format=json&lat=' + position.coords.latitude + '&lon=' + position.coords.longitude
        await axios
          .get(url)
          .then((response) => {
            console.log(response.data);
            this.setState({
              address: response.data.display_name,
            })
            console.log(this.state)
          })
          .catch((error) => {
            console.log(error);
          });
      },
      (error) => {
        this.setState({
          error: error.message
        }),
          console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 36000000 }
    );
  }
  async componentDidMount() {
    this.getLocation()
  }
  async requestPermissions() {
    if (Platform.OS === 'ios') {
      Geolocation.requestAuthorization();
      Geolocation.setRNConfiguration({
        skipPermissionRequests: false,
        authorizationLevel: 'whenInUse',
      });
    }
    if (Platform.OS === 'android') {
      const result = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Permission Explanation',
          message: 'Fastrack would like to access your location for address!',
        },
      );
      if (result !== 'granted') {
        console.log('Access to location was denied');
        return;
      }
    }
  }
  data = [
    { label: "Select Vehicle ", value: 1 },
  ];
  handlePicker = data => {
    this.setState({ pickedVehicle: data });
    console.log(data)
    this.setState({ vehicle_id: data.value })
    this.state.vehicle_id = data.value
    this.props.geofencing(this.state).then(() => {
      if (typeof this.props.geofencing1.address != 'undefined' && this.props.geofencing1.address != null)
        this.setState({
          address: this.props.geofencing1.address
        });
    });
  };
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      this.setState({ token: value })
      this.props.vehicle_list(value).then(() => {
        var vehicle_list = []
        vehicle_list = this.props.vehicle_list1.map((obj) => {
          var obj1 = {
            label: obj.vehicle_no,
            value: obj.vehicle_id
          }
          return obj1
          // console.log(obj1)
        })
        this.data = vehicle_list
        this.setState({ vehicle_list: this.props.vehicle_list1 })
      })
    }
    return value
  }
  handlesubmit() {
    console.log(this.state, 'shashank new')
    this.props.savegeofencing(this.state).then(async () => {
      console.log(this.props.savegeofencing1)
      alert('success')
      // this.props.navigation.replace('DashboardMap')
    })
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Geofencing</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.pickerBox}>
              <Image source={Images.car_white} style={styles.carIcon} />
              <Picker
                onItemChange={this.handlePicker}
                style={styles.picker}
                items={this.data}
                placeholder="All Vehicles"
                placeholderStyle={{ color: '#fff' }}
                item={this.state.pickedVehicle}
              />
              <Image source={Images.white_dropdown} style={styles.dropdown} />
            </View>
            <View style={styles.formBox}>
              <TextInput style={styles.formshortInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={(address) => { this.setState({ address }) }}
                value={this.state.address}
                placeholder="Enter Address"
              />
            </View>
            <View style={{ marginTop: '10%' }}>
              <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Select Distance</Text>
            </View>
            <View style={{ marginTop: '10%' }}>
              <Slider
                style={{ width: 350 }}
                step={1}
                minimumValue={this.state.minDistance}
                maximumValue={this.state.maxDistance}
                value={this.state.radius}
                onValueChange={val => { this.setState({ radius: val }) }}
                // thumbTintColor='rgb(252, 228, 149)'
                thumbTintColor='black'
                maximumTrackTintColor='#d3d3d3'
                minimumTrackTintColor='black'
              />
              <View style={styles.textCon}>
                <Text style={styles.colorGrey}>{this.state.minDistance} km</Text>
                <Text style={styles.colorYellow}>
                  {this.state.radius + 'km'}
                </Text>
                <Text style={styles.colorGrey}>{this.state.maxDistance} km</Text>
              </View>
            </View>
            <TouchableOpacity style={styles.submit} onPress={() => this.handlesubmit()}>
              <Text style={styles.submitText}>SUBMIT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    vehicle_list1: state.vehicle_list.vehicle_list,
    geofencing1: state.geofencing.geofencing,
    savegeofencing1: state.savegeofencing.savegeofencing,
  };
};
export default connect(mapStateToProps, {
  vehicle_list, geofencing, savegeofencing
})(Geofencing);
