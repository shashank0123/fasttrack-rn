const initialState = {
  vehicle_types: []
};

export const vehicle_types = (state = initialState, action) => {
  switch (action.type) {
    case 'vehicle_types': {
      return { ...state, vehicle_types: action.payload };
    }
    default: {
      return state;
    }
  }
};
export default vehicle_types;