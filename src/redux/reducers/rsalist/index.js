const initialState = {
  rsalist: []
};

export const rsalist = (state = initialState, action) => {
  switch (action.type) {
    case 'rsalist': {
      return { ...state, rsalist: action.payload };
    }
    default: {
      return state;
    }
  }
};
export default rsalist;