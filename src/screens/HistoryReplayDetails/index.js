import React, { Component, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  SafeAreaView
} from "react-native";
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Images } from '../../utils';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { historyreplay } from '../../redux/actions/historyreplay';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
class HistoryReplayDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      vehicle_id: this.props.route.params.vehicle_id,
      date: this.props.route.params.date,
      from_time: this.props.route.params.from_time,
      to_time: this.props.route.params.to_time,
    }
    this._getStorageValue()
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    this.setState({ token: value })
    this.props.historyreplay(this.state).then(() => {
      console.log(this.props.historyreplay1)

      this.setState({ historyreplay: this.props.historyreplay1, loading: false })
    })
    return value
  }
  render() {
    var mapStyle = [
      { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#F9E9D0" }] },
    ];
    if (this.state.loading) {
      return <View style={styles.container}>
        <View style={styles.wrapper}>
          {/* <View style={styles.upperBar}>
                  <View style={styles.backIconContainer}>
                      <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home"); }}>
                          <Image source={Images.back} style={styles.barMenuIcon} />
                      </TouchableOpacity>
                  </View>
                  {/* <View style={styles.headingContainer}>
                      <Text style={styles.barText}>Trip Information</Text>
                  </View> */}
          {/* </View> */}
          <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: height * 0.35 }}>
            <Image source={Images.logo_new2} style={{ width: width * 0.2, height: width * 0.2 }} />
            <Text style={{ fontSize: 16, marginTop: 20 }}>Loading Data...</Text>
            <Text style={{ fontSize: 16, marginTop: 20 }}> Please wait for 5 minutes...</Text>
          </View>
        </View>
      </View>
    }
    else
      return (
        <SafeAreaView style={styles.container}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>History Replay</Text>
            </View>
          </View>
          <View style={{ width: width * 1 }} >
            <MapView
              region={{
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
              showsTraffic={false}
              // mapType={'satellite'}
              showsUserLocation
              paddingAdjustmentBehavior={'always'}
              customMapStyle={mapStyle}
              mapPadding={{
                // top: 50,
                right: 50,
                bottom: 100,
                left: 50
              }}
              style={styles.maparea}
              ref={ref => this.map = ref}
            >
              <Marker
                coordinate={{ latitude: 37.78825, longitude: -122.4324, }}
                title={'Current Location'}
              />
            </MapView>
            <View style={styles.speed_text_wrapper}>
              <Text style={styles.speed_text}>0 km</Text>
              <Text style={styles.speed_label}> km/h</Text>
            </View>
            <View style={styles.play_wrapper}>
              <Image source={Images.play} style={{ width: 30, height: 30 }} />
            </View>
          </View>
          <View style={{ position: 'absolute', top: height * 0.85 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', }}>
              <Text style={{ marginLeft: '10%' }}>Date : {this.state.date}</Text>
              <Text style={{ marginLeft: '10%' }}>Distance : {this.props.historyreplay1.distance} KM</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, }}>
              <Text style={{ marginLeft: '10%' }}>Start time : {this.state.from_time}</Text>
              <Text style={{ marginLeft: '10%' }}>End time : {this.state.to_time}</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
              <Text style={{ marginLeft: '10%' }}>Avg speed : {this.props.historyreplay1.avg_speed}</Text>
              <Text style={{ marginLeft: '10%' }}>Max speed : {this.props.historyreplay1.max_speed} KM/Hr</Text>
            </View>
          </View>
        </SafeAreaView>
      );
  }
}
const mapStateToProps = (state) => {
  return {
    historyreplay1: state.historyreplay.historyreplay,
  };
};
export default connect(mapStateToProps, {
  historyreplay
})(HistoryReplayDetails);
