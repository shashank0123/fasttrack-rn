import axios from 'axios';
import urlObj from '../../url';
export const engine_off = (data) => {
  return async (dispatch, getState) => {

    url = urlObj.engine_off_url + "?token=" + data.token + "&vehicle_id=" + data.vehicle_id;
    console.log(url)
    await axios
      .post(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'LOGIN',
          payload: response.data.message

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};