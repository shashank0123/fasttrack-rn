import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Image, SafeAreaView
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { registeration } from '../../redux/actions/registeration';
import { vehicle_types } from '../../redux/actions/vehicle_types';

class AddMainUser extends React.Component {
  data = [
    { label: "Select Vehicle ", value: 1 },
  ];
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      selectedValue: '',
      pickedVehicle: '',
      vehicle_types: [],
      selectedItems: [],
      sub_user: [],
      device_no: '',
      sim_no: '',
      vehicle_no: '',
      vehicle_type: '',
      user_name: '',
      mobile_no: ''
    }
    this._getStorageValue()
  }

  submitUser() {
    this.props.registeration(this.state).then(() => {
      alert(this.state.registeration1)
      // this.props.navigation.replace('Home')
    })
  }

  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })
      this.props.vehicle_types().then(() => {
        console.log(this.props.vehicle_types1, 'shashank2')
        this.data = this.props.vehicle_types1
        this.setState({ vehicle_type: this.props.vehicle_types1[0].value })
        if (this.data.length > 0)
          this.state.pickedVehicle = this.data[0]
        console.log(this.state.pickedVehicle, 'shashank')
      })
    }
    return value
  }

  handlePicker = data => {
    this.setState({ pickedVehicle: data });
    console.log(data)
    this.setState({ vehicle_type: data.value })
  };



  render() {
    const { selectedItems } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Registeration</Text>
            </View>
          </View>
          <KeyboardAvoidingView behavior="padding">
            <ScrollView
              contentContainerStyle={styles.scrollWrapper}
              showsVerticalScrollIndicator={false}>
              <View style={styles.formWrapper}>

                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    value={this.state.device_no}
                    onChangeText={(device_no) => { this.setState({ device_no }) }}
                    placeholder="Device Number" />
                </View>

                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    value={this.state.sim_no}
                    onChangeText={(sim_no) => { this.setState({ sim_no }) }}
                    placeholder="Sim Number" />
                </View>

                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    value={this.state.vehicle_no}
                    onChangeText={(vehicle_no) => { this.setState({ vehicle_no }) }}
                    placeholder="Vehicle Number" />
                </View>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handlePicker}
                    style={styles.picker}
                    items={this.data}
                    placeholder="Vehicle Type"
                    placeholderStyle={{ color: '#000' }}
                    item={this.state.pickedVehicle}
                  />
                  <Image source={Images.white_dropdown} style={styles.dropdown} />
                </View>


                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    onChangeText={(user_name) => { this.setState({ user_name, email: user_name }) }}
                    autoCapitalize="none"
                    value={this.state.user_name}
                    placeholder="User Name" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    value={this.state.mobile_no}
                    onChangeText={(mobile_no) => { this.setState({ mobile_no }) }}
                    keyboardType='numeric'
                    maxLength={10}
                    placeholder="Mobile Number" />
                  <Image source={Images.call_Icon} style={styles.callIcon} />
                </View>

                <View style={{ marginTop: 5, justifyContent: 'center', alignItems: 'center', marginLeft: 30 }}>
                  {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}
                </View>
                {/* </View> */}

                <TouchableOpacity style={styles.submit} onPress={() => { this.submitUser() }}>
                  <Text style={styles.submitText}>Submit</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    registeration1: state.registeration.registeration,
    vehicle_types1: state.vehicle_types.vehicle_types
  };
};
export default connect(mapStateToProps, {
  registeration, vehicle_types
})(AddMainUser);

