import axios from 'axios';
import urlObj from '../../url';
export const geofencing = (state) => {
  return async (dispatch, getState) => {

    url = urlObj.geofencing_url + '?token=' + state.token + '&vehicle_id=' + state.vehicle_id;
    console.log(url)


    await axios
      .get(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'GEOFENCING',
          payload: response.data.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};