const initialState = {
    complaint: []
  };
  
  export const complaint = (state = initialState, action) => {
    switch (action.type) {
      case 'complaint': {
        // console.log(action.payload)
        return { ...state,complaint:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default complaint;