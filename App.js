import React, { Component, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
// import firebase from 'react-native-firebase';

import { store } from './src/redux/store';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Provider } from 'react-redux';
import { fcmService } from './src/utils/FCMService'
import { localNotificationService } from './src/utils/LocalNotificationService'

import 'react-native-gesture-handler';
import Navigation from "./src/navigation";

export default function App() {

  // cons

  useEffect(() => {

    fcmService.registerAppWithFCM()
    fcmService.register(onRegister, onNotification, onOpenNotification)
    localNotificationService.configure(onOpenNotification)
    console.log(fcmService)

    function onRegister(token) {
      console.log("onRegister", token)
    }

    function onNotification(notify) {
      console.log("onNotification ", notify)
      const options = {
        soundName: "default",
        playSound: true
      }

      localNotificationService.showNotification(
        0,
        notify.title,
        notify.body,
        notify,
        options
      )
    }

    function onOpenNotification(notify) {
      console.log("[App], onOpenNotification", notify)
      alert("Open Notification", notify.body)
    }

    return () => {
      console.log("[App] unregister")
      // fcmService.unRegister()
      // localNotificationService.unRegister()
    }

  }, [])



  return (
    <Provider store={store}>

      <Navigation />
    </Provider>
  );

};
