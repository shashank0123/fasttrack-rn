import axios from 'axios';
import urlObj from '../../url';
export const complaint = (state) => {
  return async (dispatch, getState) => {
    
    url = urlObj.complaint_url+"?token="+state.token; 
var data = new FormData()
data.append('complaint', state.complaint)
data.append('mobile', state.mobile)
data.append('name', state.name)
data.append('vehicle_no', state.vehicle_no)
		console.log(url)
    await axios
      .post(url, data)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'complaint',
          payload: response.data.complaint
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};