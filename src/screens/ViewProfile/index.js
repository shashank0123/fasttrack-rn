import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Button,
  Modal,
  SafeAreaView,
} from "react-native";
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import styles from './style';
import { Images } from '../../utils';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { profile } from '../../redux/actions/profile';
var language = [
  { label: 'English', value: 'en' },
  { label: 'हिन्दी', value: 'hi' },
]
class ViewProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showLanguageModal: false,
      selected_language: '',
      languageRadioInitial: 0,
      data: {}
    }
    this._getStorageValue()
  }
  async logout() {
    await AsyncStorage.removeItem('token')
    await AsyncStorage.removeItem('fcmToken')
    this.props.navigation.navigate('Login')
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.props.profile(value).then(() => {
        console.log(this.props.profile1)
        this.setState({
          data: this.props.profile1,
          loading: false,
          viewAnimation: true,
        });
      })
    }
    return value
  }
  languageRadioChange(value) {
    this.setState({
      selected_language: value,
      showLanguageModal: false
    })
    if (value == 'en')
      this.setState({ languageRadioInitial: 0 })
    else if (value == 'hi')
      this.setState({ languageRadioInitial: 1 })
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>View Profile</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.profileImgWrapper}>
              <Image source={Images.drawer_profile} style={styles.profileImg} />
            </View>
            <View style={styles.listWrapper}>
              <View style={styles.field}>
                <Image source={Images.person_Icon} style={styles.icon} />
                <Text style={styles.fieldText}>{this.state.data.name}</Text>
              </View>
              <View style={styles.field}>
                <Image source={Images.call_Icon} style={styles.icon} />
                <Text style={styles.fieldText}>{this.state.data.mobile}</Text>
              </View>
              <View style={styles.field}>
                <Image source={Images.date_calendar} style={styles.icon} />
                <Text style={styles.fieldText}>{this.state.data.dob}</Text>
              </View>
              <View style={styles.field}>
                <Image source={Images.gender} style={styles.icon} />
                <Text style={styles.fieldText}>{this.state.data.gender}</Text>
              </View>
              <View style={styles.field}>
                <Image source={Images.email} style={styles.icon} />
                <Text style={styles.fieldText}>{this.state.data.email}</Text>
              </View>
              <TouchableOpacity style={styles.field}
                onPress={() => { this.props.navigation.navigate("ChangePassword") }}>
                <Image source={Images.change_password} style={styles.icon} />
                <Text style={styles.fieldText2}>Change Password</Text>
                <Image source={Images.grey_arrow_left} style={styles.arrowicon} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.field}
                onPress={() => { this.props.navigation.navigate("EditProfile") }}>
                <Image source={Images.gender} style={styles.icon} />
                <Text style={styles.fieldText2}>Edit Profile</Text>
                <Image source={Images.grey_arrow_left} style={styles.arrowicon} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.field}
                onPress={() => { this.setState({ showLanguageModal: true }) }}>
                <Image source={Images.change_language} style={styles.icon} />
                <Text style={styles.fieldText2}>Change Language</Text>
                <Image source={Images.grey_arrow_left} style={styles.arrowicon} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.field}
                onPress={() => { this.logout() }}>
                <Image source={Images.logout} style={styles.icon} />
                <Text style={styles.fieldText2}>Log Out</Text>
                <Image source={Images.grey_arrow_left} style={styles.arrowicon} />
              </TouchableOpacity>
            </View>
          </View>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showLanguageModal}>
            <TouchableOpacity style={styles.modalScreenWrapper} onPress={() => { this.setState({ showLanguageModal: false }) }}>
              <View style={styles.modalWrapper}>
                <Text style={styles.modalText}>Choose Language</Text>
                <RadioForm style={styles.radioList} buttonSize={15}
                  labelStyle={styles.radioLabel}
                  radio_props={language}
                  initial={this.state.languageRadioInitial}
                  onPress={(value) => { this.languageRadioChange(value) }} />
              </View>
            </TouchableOpacity>
          </Modal>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    profile1: state.profile.profile
  };
};
export default connect(mapStateToProps, {
  profile
})(ViewProfile);
