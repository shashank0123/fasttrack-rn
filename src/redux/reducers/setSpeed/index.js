const initialState = {
    setSpeed: {}
  };
  
  export const setSpeed = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        // console.log(action.payload)
        return { ...state,setSpeed:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default setSpeed;