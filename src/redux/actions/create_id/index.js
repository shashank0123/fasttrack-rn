import axios from 'axios';
import urlObj from '../../url';
export const create_id = (state) => {
  return async (dispatch, getState) => {

    url = urlObj.create_id_url + "?token=" + state.token;;
    var data = new FormData()
    data.append('name', state.name)
    data.append('mobile', state.mobile)
    data.append('email', state.email)
    data.append('password', state.password)
    data.append('vehicle_id', state.vehicle_id)
    console.log(data)
    await axios
      .post(url, data)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'create_id',
          payload: response.data.create_id

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};