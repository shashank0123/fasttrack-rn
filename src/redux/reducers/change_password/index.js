const initialState = {
    changepassword: {}
  };
  
  export const changepassword = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        // console.log(action.payload)
        return { ...state,changepassword:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default changepassword;