import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  SafeAreaView,
} from 'react-native';
import styles from './style';
import { Images } from '../../utils';
import call from 'react-native-phone-call';
import { connect } from 'react-redux';
import { rsalist } from '../../redux/actions/rsalist';
class RSA extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      companies: [],
    };
    this.getData();
  }

  getData() {
    this.props.rsalist().then(() => {
      console.log(this.props.rsalist1);
      this.setState({ companies: this.props.rsalist1 });
    });
  }

  onCall(number) {
    if (!number) {
      return;
    }
    const args = {
      number: number,
      prompt: false,
    };
    call(args).catch(console.error);
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Road Side Assistance</Text>
            </View>
          </View>
          <View style={styles.flatlistWrapper}>
            <FlatList
              showsVerticalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              data={this.state.companies}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.cardWrapper}
                  onPress={() => this.onCall(item.contact)}>
                  <View style={styles.card}>
                    <Text>{item.company}</Text>
                    <Image source={Images.call} style={styles.callIcon} />
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    rsalist1: state.rsalist.rsalist,
  };
};
export default connect(mapStateToProps, {
  rsalist,
})(RSA);
