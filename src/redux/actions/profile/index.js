import axios from 'axios';
import urlObj from '../../url';
export const profile = (token) => {
  return async (dispatch, getState) => {
    
    url = urlObj.profile_url+"?token="+token;
    await axios
      .get(url)
      .then((response) => {
        dispatch({
          type: 'LOGIN',
          payload: response.data.profile
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};