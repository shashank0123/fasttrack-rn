const initialState = {
    vehicle_list: []
  };
  
  export const vehicle_list = (state = initialState, action) => {
    switch (action.type) {
      case 'vehicle_list': {
        // console.log(action.payload)
        return { ...state,vehicle_list:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default vehicle_list;