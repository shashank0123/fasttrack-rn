import axios from 'axios';
import urlObj from '../../url';
export const savegeofencing = (state) => {
  return async (dispatch, getState) => {

    url = urlObj.geofencing_url + "?token=" + state.token;
    var data = new FormData()
   
    data.append('vehicle_id', state.vehicle_id)
    data.append('address', state.address)
    data.append('latitude', state.latitude)
    data.append('longitude', state.longitude)
    data.append('radius', state.radius)
    data.append('status', state.status)
  
    console.log(data)
    await axios
      .post(url, data)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'SAVEGEOFENCING',
          payload: response.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};