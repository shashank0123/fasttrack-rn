import axios from 'axios';
import urlObj from '../../url';
export const delete_id = (token, user_id) => {
  return async (dispatch, getState) => {

    url = urlObj.delete_id_url + "?token=" + token;
    var data = new FormData()
    data.append('user_id', user_id)
    console.log(url)
    await axios
      .post(url, data)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'delete_id',
          payload: response.data.delete_id

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};