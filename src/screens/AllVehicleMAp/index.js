import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  ScrollView,
  SafeAreaView,
  Alert
} from "react-native";
const { width, height } = Dimensions.get('window');
import styles from './style';
import MapView, { AnimatedRegion, Marker, Polyline } from "react-native-maps";
import KeepAwake from 'react-native-keep-awake';
import { Images } from '../../utils';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faMotorcycle, faCar, faTruck, faTruckPlow, faBus, faTractor } from '@fortawesome/free-solid-svg-icons'
const LATITUDE_DELTA = 0.0009;
const LONGITUDE_DELTA = 0.0009;
class AllVehicleMap extends React.Component {
  constructor(props) {
    super(props);
    let initial_date = new Date;
    this.state = {
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
      latitude: 0,
      token: '',
      longitude: 0,
      error: null,
      origin: null,
      vehicle: null,
      user_id: this.props.route.params.user_id,
      vehicle_id: 0,
      points: [],
      timer: null,
      address: 1,
      time: new Date(initial_date.getTime())
    };
    this._getStorageValue()
  }
  state = { vehicle_list: [] }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value });
      this.setState({ vehicle_id: 0 });
    }
    return value
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  componentDidMount() {
    this.timerID = setInterval(() => {
      var address = 1;
      var endDate = new Date();
      var seconds = (endDate.getTime() - this.state.time.getTime()) / 1000;
      if (seconds < 20) {
        address = 2
      }
      fetch('http://fasttrackgpsinfo.com/api/show_location3?address=' + this.state.address + '&user_id=1490&vehicle_id=' + this.state.vehicle_id + '&address=' + address, {
        method: 'GET'
      })
        .then(res => res.json())
        .then(data => {
          if (data.status == 200 && data.location.length > 0) {
            if (this.state.origin == null) {
              this.setState({
                origin: data.location[0]
              });
            }
            this.setState({
              vehicle: data.location[0]
            });
            this.setPoint(data.location[0]);
            this.calculateMapRegion();
          } else {
            this.setState({
              vehicle: null
            });
          }//anwarali5/12345
        })
        .catch(err => {
        });
    }, 5000);
  }
  setPoint(item) {
    var lat = Number(item.latitude);
    var lng = Number(item.longitude);
    var isExist = false;
    for (var i = 0; i < this.state.points.length; i++) {
      if (this.state.points[i].latitude == lat && this.state.points[i].longitude == lng) {
        isExist = true;
        break;
      }
    }
    if (isExist == false) {
      this.setState({
        points: [...this.state.points, {
          latitude: lat,
          longitude: lng,
          heading: 350
        }]
      });
      this.doAnimattion({
        latitude: lat,
        longitude: lng,
        heading: 350
      })
    }
  }
  onBack() {
    this.props.navigation.replace('Home');
  }
  onRefrsh() {
  }
  onMarkerPress = () => {
    this.setState({ time: new Date() })
    console.log('vehicle_no', 'clicked')
  }
  showMarker(item, isOrigin) {
    if (item == null) {
      return;
    }
    var coordinate = {
      longitude: Number(item.longitude) ? Number(item.longitude) : 0,
      latitude: Number(item.latitude) ? Number(item.latitude) : 0,
    };
    if (isOrigin == true) {
      return (
        <Marker.Animated
          coordinate={coordinate}
          onPress={(e) => { e.stopPropagation(); this.onMarkerPress() }}
          anchor={{ x: 0.5, y: 0.5 }}
          style={{
            width: 16, height: 18, transform: [{
              rotate: item.heading ? item.heading : '0deg'
            }]
          }}
        />
      );
    } else {
      return (
        <Marker.Animated
          coordinate={coordinate}
          anchor={{ x: 0.5, y: 0.5 }}
          onPress={(e) => { e.stopPropagation(); this.onMarkerPress() }}
          image={item.color == 'green' && item.vehicle_type == 'Bike' ? Images.bike_green : item.color == 'red' && item.vehicle_type == 'Bike' ? Images.bike_red : item.color == 'blue' && item.vehicle_type == 'Bike' ? Images.bike_blue : item.color == 'yellow' && item.vehicle_type == 'Bike' ? Images.bike_yellow : item.color == 'green' && item.vehicle_type == 'Motor Car' ? Images.car_green : item.color == 'red' && item.vehicle_type == 'Motor Car' ? Images.car_red : item.color == 'yellow' && item.vehicle_type == 'Motor Car' ? Images.car_yellow : item.color == 'blue' && item.vehicle_type == 'Motor Car' ? Images.car_blue : item.color == 'green' && item.vehicle_type == 'Bus' ? Images.bus_green : item.color == 'yellow' && item.vehicle_type == 'Bus' ? Images.bus_yellow : item.color == 'red' && item.vehicle_type == 'Bus' ? Images.bus_red : Images.car_red}
          style={{
            width: 16, height: 18, transform: [{
              rotate: item.heading ? item.heading : '0deg'
            }]
          }}
          resizeMode="contain"
        />
      );
    }
  }
  openMap = () => {
    console.log('open directions')
    Platform.select({
      ios: () => {
        Linking.openURL('http://maps.apple.com/maps?daddr=');
      },
      android: () => {
        Linking.openURL('http://maps.google.com/maps?daddr=');
      }
    });
  }
  showLine() {
    return (
      <Polyline
        coordinates={this.state.points}
        strokeColor="black"
        strokeColors={[
          '#7F0000',
          '#B24112',
          '#E5845C',
          '#238C23',
          '#7F0000'
        ]}
        strokeWidth={3}
      />
    );
  }
  calculateMapRegion() {
    return this.getRegionForCoordinates(this.state.points)
  }
  getRegionForCoordinates(points) {
    // points should be an array of { latitude: X, longitude: Y }
    let minX, maxX, minY, maxY;
    // init first point
    ((point) => {
      minX = point.latitude;
      maxX = point.latitude;
      minY = point.longitude;
      maxY = point.longitude;
    })(points[0]);
    // calculate rect
    points.map((point) => {
      minX = Math.min(minX, point.latitude);
      maxX = Math.max(maxX, point.latitude);
      minY = Math.min(minY, point.longitude);
      maxY = Math.max(maxY, point.longitude);
    });
    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;
    const deltaX = (maxX - minX);
    const deltaY = (maxY - minY);
    this.setState({
      latitudeDelta: deltaX,
      longitudeDelta: deltaY,
      longitude: midY,
      latitude: midX
    });
    return {
      latitude: midX,
      longitude: midY,
      latitudeDelta: deltaX,
      longitudeDelta: deltaY
    };
  }
  render() {
    var mapStyle = [
      { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#F9E9D0" }] },
    ];
    return (
      <SafeAreaView style={styles.container}>
        {/*header bar: begin*/}
        <View style={styles.header_container}>
          <TouchableOpacity onPress={() => this.onBack()}>
            <View style={styles.back_btn}>
              <Image source={Images.icon_back_blue} style={styles.back_btn_img} />
              <Text style={styles.back_btn_text}>All Vehicle Map</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.top_btns_container}>
            <TouchableOpacity style={styles.top_btn} onPress={() => this.onRefrsh()}>
              <Image source={Images.refresh_blue} style={styles.top_btn_image} />
            </TouchableOpacity>
          </View>
        </View>
        {/*header bar: end*/}
        <MapView
          region={{
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
            longitude: this.state.longitude,
            latitude: this.state.latitude
          }}
          showsTraffic={true}
          provider={null}
          toolbarEnabled={true}
          paddingAdjustmentBehavior={'always'}
          customMapStyle={mapStyle}
          mapPadding={{
            top: 50,
            right: 50,
            bottom: 100,
            left: 50
          }}
          style={styles.maparea}
          ref={ref => this.map = ref}
        >
          {this.showLine()}
          {this.showMarker(this.state.origin, true)}
          {this.showMarker(this.state.vehicle, false)}
        </MapView>
        <KeepAwake />
      </SafeAreaView >
    );
  }
}
const mapStateToProps = (state) => {
  return {
  };
};
export default connect(mapStateToProps, {
})(AllVehicleMap);
