import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');
export default StyleSheet.create({
    container: {
        width,
        height,
        position: 'relative',
        color:'#fff'
    },
    wrapper: {
        width,
        height,
        display: 'flex',
        justifyContent: 'flex-start',
    },
    upperBar: {
        width,
        height: height * 0.075,
        backgroundColor: '#000000',
        flexDirection: 'row',
        alignItems: 'center',
    },
    backIconContainer: {
    },
    headingContainer: {
        width: '60%',
        marginLeft: '10%',
        alignItems: 'center',
    },
    barText: {
        color: "#ffffff",
        fontSize: height * 0.03,
    },
    barMenuIcon: {
        height: height * 0.04,
        width: height * 0.04,
        marginLeft: width * 0.04,
    },
    boxBtn2: {
        width: width * 0.4,
        height: height * 0.065,
        display: 'flex',
        backgroundColor: '#006631',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
    },
    btnText: {
        // width:'100%',
        // fontWeight: 'bold',
        textShadowColor: '#ffffff',
        fontSize: 18,
        color: '#ffffff'
    },
    barDetailImg: {
        width: width * 1,
        height:height*0.75,
        // borderRadius: 50,
        // borderWidth: 1,
        borderColor: '#ffffff',
        shadowColor: '#000000',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 1,
        marginRight: 10,
        // elevation: 5,
        marginBottom: 4
    },
    speed_text_wrapper: {
        width: 60,
        height: 60,
        backgroundColor: 'black',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left:20,
        top:500,
        borderRadius: 35,
        zIndex: 10000
      },
      play_wrapper: {
        width: 60,
        height: 60,
        // backgroundColor: 'black',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        left:320,
        top:500,
        borderRadius: 35,
        zIndex: 10000
      },
    
      speed_text: {
        fontSize:12, 
        color: 'white'
      },
    
      speed_label: {
        fontSize: 10, 
        color: 'white'
      },
      maparea: {
        width: width,
        height:height*0.75,
        // height:height*0.5,
        alignItems: 'flex-start'
    },
    
  

});
