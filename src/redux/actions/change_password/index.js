import axios from 'axios';
import urlObj from '../../url';
export const changepassword = (state) => {
  return async (dispatch, getState) => {
    
    url = urlObj.change_password_url+"?token="+state.token+ "&old_password=" + state.old_password + "&new_password=" + state.new_password  ;
    await axios
      .post(url)
      .then((response) => {
        dispatch({
          type: 'LOGIN',
          payload: response.data
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};