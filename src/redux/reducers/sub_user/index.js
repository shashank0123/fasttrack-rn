const initialState = {
  sub_user: [],
  sub_user_status: {}
};

export const sub_user = (state = initialState, action) => {
  switch (action.type) {
    case 'sub_user': {
      return { ...state, sub_user: action.payload };
    }
    case 'STATUS': {
      return { ...state, sub_user_status: action.payload };
    }
    default: {
      return state;
    }
  }
};
export default sub_user;