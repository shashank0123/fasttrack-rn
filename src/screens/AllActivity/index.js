import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  SafeAreaView
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
import CheckBox from '@react-native-community/checkbox';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { all_activity } from '../../redux/actions/all_activity';
const companies = [
  {
    id: 1,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },
  {
    id: 2,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },
  {
    id: 3,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },
  {
    id: 4,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },
  {
    id: 5,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },
  {
    id: 6,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },
  {
    id: 7,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },
  {
    id: 8,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },
  {
    id: 9,
    vech_no: 'HR07M0528 Engine turned off',
    status: ' Change in Engine Status',
    date: '2021-02-01',
    time: '13:39:35'
  },

]
class AllActivity extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      all_activity: [],
      vehicle: null,
    }
    this._getStorageValue()
  }

  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })
      this.setState({ vehicle_id: this.props.route.params.vehicle_id });
      this.props.all_activity(value, this.props.route.params.vehicle_id).then(() => {



        this.data = all_activity

        this.setState({ all_activity: this.props.all_activity1 })
      })


    }
    return value
  }

  render() {
    console.log(this.props.all_activity1)
    return (
      <SafeAreaView style={styles.container}>


        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.back} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text style={styles.barText}>All Activity</Text>
          </View>
        </View>

        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.flatlistWrapper}
          keyExtractor={(item, index) => index.toString()}
          data={this.state.all_activity}
          renderItem={({ item }) =>


            <View style={styles.docDetailedWrapper2}>
              <View
                style={[styles.docSpecsWrapper, { marginTop: 10 }]}>


                <View style={styles.docNameWrapper}>
                  <View style={{ flexDirection: 'row' }}>
                    <Image source={Images.person_Icon} style={styles.cardIcon} />
                    <Text numberOfLines={1} style={styles.docNameText}>
                      {item.title}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Image source={Images.person_Icon} style={styles.cardIcon} />
                    <Text numberOfLines={1} style={styles.docNameText}>
                      {item.description}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Image source={Images.person_Icon} style={styles.cardIcon} />
                    <Text numberOfLines={1} style={styles.docNameText}>
                      {item.created_at}
                    </Text>
                  </View>


                </View>


              </View>
            </View>


          } />


      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    all_activity1: state.all_activity.all_activity
  };
};
export default connect(mapStateToProps, {
  all_activity
})(AllActivity);

