const initialState = {
    create_id: []
  };
  
  export const create_id = (state = initialState, action) => {
    switch (action.type) {
      case 'create_id': {
        // console.log(action.payload)
        return { ...state,create_id:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default create_id;