import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Button,
  SafeAreaView,
} from "react-native";

import styles from './style';
import { Images } from '../../utils';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { changepassword } from '../../redux/actions/change_password';
class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',
      hideConfirmPassword: true,
      hideCurrentPassword: true,
      hideNewPassword: true,
      old_password: '',
      new_password: ''
    }
    this._getStorageValue()
  }

  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })

    }
    return value
  }
  onSubmit() {
    // console.log(this.state)
    this.props.changepassword(this.state).then(async () => {
      console.log(this.props.changepassword1)
      this.props.navigation.replace('ViewProfile')
    })
    // this.showAlert();
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Change Password</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.formBox}>
              <TextInput style={styles.formshortInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={() => { }}
                placeholder="Current Password"
                onChangeText={old_password => this.setState({ old_password })}
                value={this.state.old_password}
                secureTextEntry={this.state.hideCurrentPassword} />
              <TouchableOpacity onPress={() => { this.setState({ hideCurrentPassword: !(this.state.hideCurrentPassword) }) }}>
                <Image source={(this.state.hideCurrentPassword) ? (Images.eye_line) : (Images.eye)} style={styles.searchIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.formBox}>
              <TextInput style={styles.formshortInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={() => { }}
                placeholder="New Password"
                onChangeText={new_password => this.setState({ new_password })}
                value={this.state.new_password}
                secureTextEntry={this.state.hideNewPassword} />
              <TouchableOpacity onPress={() => { this.setState({ hideNewPassword: !(this.state.hideNewPassword) }) }}>
                <Image source={(this.state.hideNewPassword) ? (Images.eye_line) : (Images.eye)} style={styles.searchIcon} />
              </TouchableOpacity>
            </View>
            {/* <View style={styles.formBox}>
              <TextInput style={styles.formshortInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={() => { }}
                placeholder="Confirm Password"
                secureTextEntry={this.state.hideConfirmPassword} />
              <TouchableOpacity onPress={() => { this.setState({ hideConfirmPassword: !(this.state.hideConfirmPassword) }) }}>
                <Image source={(this.state.hideConfirmPassword) ? (Images.eye_line) : (Images.eye)} style={styles.searchIcon} />
              </TouchableOpacity>
            </View> */}
            <TouchableOpacity style={styles.submit} onPress={() => { this.onSubmit() }}>
              <Text style={styles.submitText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {

    changepassword1: state.changepassword.changepassword
  };
};
export default connect(mapStateToProps, {
  changepassword
})(ChangePassword);

