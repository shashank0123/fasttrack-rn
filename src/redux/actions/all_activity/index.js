import axios from 'axios';
import urlObj from '../../url';
export const all_activity = (token,vehicle_id) => {
  return async (dispatch, getState) => {
    
    url = urlObj.all_activity_url+"?token="+token+"&vehicle_id="+vehicle_id;
    console.log(url)
    await axios
      .get(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'all_activity',
          payload: response.data.caractivity
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};