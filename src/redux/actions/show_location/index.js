import axios from 'axios';
import urlObj from '../../url';
export const login = (id) => {
  return async (dispatch, getState) => {
    
    url = urlObj.login_url+"?email="+login.email+"&password="+login.password+"&device_token="+login.device_token+"&mobile="+login.mobile+"&imei="+login.imei;
    console.log(url)
    await axios
      .post(url)
      .then((response) => {
        console.log(response)
        dispatch({
          type: 'LOGIN',
          payload: response.data.data
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};