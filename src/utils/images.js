export default {
  back: require('../assets/back.png'),
  bg_map: require('../assets/bg_map.png'),
  bike_white: require('../assets/bike_white.png'),
  bike_blue: require('../assets/bike_blue.png'),
  bus_blue: require('../assets/bus_blue.png'),
  bus_green: require('../assets/bus_green.png'),
  bus_red: require('../assets/bus_red.png'),
  bus_yellow: require('../assets/bus_yellow.png'),
  bike_green: require('../assets/bike_green.png'),
  bike_red: require('../assets/bike_red.png'),
  bike_yellow: require('../assets/bike_yellow.png'),
  call: require('../assets/call.png'),
  car_green: require('../assets/car_green.png'),
  car_red: require('../assets/car_red.png'),
  car_blue: require('../assets/car_blue.png'),
  car_white: require('../assets/ic_car_white.png'),
  car_yellow: require('../assets/car_yellow.png'),
  car_parked_b: require('../assets/car_parked_b.png'),
  call_Icon: require('../assets/calIcon2.png'),
  change_password: require('../assets/change_pasword_blue.png'),
  change_language: require('../assets/language.png'),
  drawer_profile: require('../assets/profile_default.png'),
  drawer_user: require('../assets/ic_user.png'),
  drawer_setting: require('../assets/settings.png'),
  drawer_home: require('../assets/home_white.png'),
  drawer_sl: require('../assets/ic_odo_meter.png'),
  drawer_hr: require('../assets/clock_n.png'),
  drawer_complaint: require('../assets/ic_demo_white.png'),
  drawer_logout: require('../assets/ic_info_white.png'),
  drawer_report: require('../assets/news_feed_w.png'),
  drawer_distance: require('../assets/location_start_point.png'),
  drawer_trip: require('../assets/ic_car_trip.png'),
  date_calendar: require('../assets/calender.png'),
  email: require('../assets/mail.png'),
  engine_b: require('../assets/engine_b.png'),
  eye: require('../assets/eye.png'),
  eye_line: require('../assets/eye_line.png'),
  edit_pic: require('../assets/edit_pic.png'),
  gender: require('../assets/gender.png'),
  grey_arrow_left: require('../assets/grey_arrow_left.png'),
  icon_back_blue: require('../assets/icon_back_blue.png'),
  icon_signal_blue: require('../assets/icon_signal_blue.png'),
  icon_notification_blue: require('../assets/icon_notification_blue_2.png'),
  list_b: require('../assets/list_b.png'),
  location_g_new: require('../assets/location_g_new.png'),
  logout: require('../assets/logout.png'),
  logo_new2: require('../assets/logo_new2.png'),
  marker_navigation_blue: require('../assets/marker_navigation_blue.png'),
  marker_navigation_red: require('../assets/marker_navigation_red.png'),
  navigation_Menu: require('../assets/navigation_menu.png'),
  person_Icon: require('../assets/personIcon.png'),
  phone_contact_b: require('../assets/phone_contact_b.png'),
  pin_location: require('../assets/pin_location.png'),
  refresh_blue: require('../assets/refresh_blue.png'),
  refresh_blue: require('../assets/refresh_blue.png'),
  searchMenu_icon: require('../assets/searchMenu_icon.png'),
  search_grey: require('../assets/search_grey.png'),
  speed: require('../assets/speed.png'),
  sos: require('../assets/sos.png'),
  time_clock: require('../assets/clock_n.png'),
  white_dropdown: require('../assets/white_dropdown.png'),
  map: require('../assets/map.png'),
  play: require('../assets/play.png'),
  bannerBtn: require('../assets/bannerBtn.png'),
  p1: require('../assets/1.jpg'),
  p2: require('../assets/2.jpg'),
  p3: require('../assets/3.jpg'),
  p4: require('../assets/4.jpg'),
  black_dropdwn: require('../assets/dropdown.png')
}
