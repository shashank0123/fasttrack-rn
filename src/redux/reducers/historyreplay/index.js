const initialState = {
    historyreplay: {}
  };
  
  export const historyreplay = (state = initialState, action) => {
    switch (action.type) {
      case 'HISTORYREPLAY': {
        // console.log(action.payload)
        return { ...state,historyreplay:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default historyreplay;