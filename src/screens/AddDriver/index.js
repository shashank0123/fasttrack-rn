import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Button, SafeAreaView, Alert
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
import { Picker } from 'react-native-woodpicker';
import ImagePicker from 'react-native-image-crop-picker';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { adddriver } from '../../redux/actions/add_driver';
class AddDriver extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      id_image: '',
      pancard_image: '',
      aadhar_image: '',
      driver_image: '',
      driver_name: '',
      driver_phone: '',
      driver_email: '',
      driver_gander: '',
      driver_state: '',
      driver_city: '',
      driver_dob: '',
      driver_country: '',
      driver_pincode: '',
      driver_pan_no: '',
      driver_drive_licence_no: '',
      driver_aadhar_no: ''
    }
    this._getStorageValue()
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })
    }
    return value
  }
  // onSubmit() {
  //   // console.log(this.state)
  //   this.props.adddriver(this.state).then(async () => {
  //     console.log(this.props.adddriver1)
  //     this.props.navigation.replace('Home')
  //   })
  //   // this.showAlert();
  // }
  onSubmit() {
    var error = 0;
    var message = 'Something went wrong';
    if (this.state.driver_name == '') {
      error = 1;
      message = 'Enter Name'
    }
    if (this.state.driver_phone == '') {
      error = 1;
      message = 'Enter Mobile Number'
    }
    if (error != 1) {
      // console.log(this.state)
      this.props.adddriver(this.state).then(async () => {
        console.log(this.props.adddriver1)
        // this.props.navigation.replace('Home')
      })
    }
    else {
      Alert.alert(
        'Fastrack',
        message,
      )
    }
  }
  CoutryData = [
    { label: "Select Country ", value: '' },
    { label: "India", value: 1 },
    { label: "U.S.A", value: 2 },
  ];
  handleCountryPicker = data => {
    console.log(data, 'yaha h driver country')
    this.setState({ driver_country: data }, () => {
      console.log('==========>', this.state.driver_country)
    });
  };
  StateData = [
    { label: "Select State ", value: '' },
    { label: "Bihar", value: 1 },
    { label: "Delhi", value: 2 },
  ];
  handleStatePicker = data => {
    this.setState({ driver_state: data });
    console.log(data, 'yaha h driver state')
  };
  CityData = [
    { label: "Select City ", value: '' },
    { label: "Noida", value: 1 },
    { label: "Delhi", value: 2 },
  ];
  handleCityPicker = data => {
    this.setState({ driver_city: data });
    console.log(data, 'yaha h driver city')
  };
  GenderData = [
    { label: "Select Gender ", value: '' },
    { label: "Male", value: 1 },
    { label: "Female", value: 2 },
    { label: "Other", value: 3 },
  ];
  handleGenderPicker = data => {
    this.setState({ driver_gender: data });
    console.log(data, 'yaha h driver gender')
  };
  selectIdProofImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log('Image is here', image);
      this.setState({ id_image: image.path })
    });
  }
  selectPancardImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log('Image is here', image);
      this.setState({ pancard_image: image.path })
    });
  }
  selectAadharImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log('Image is here', image);
      this.setState({ aadhar_image: image.path })
    });
  }
  selectDriverImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log('Image is here', image);
      this.setState({ driver_image: image.path })
    });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Add Driver</Text>
            </View>
          </View>
          <KeyboardAvoidingView behavior="padding">
            <ScrollView
              contentContainerStyle={styles.scrollWrapper}
              showsVerticalScrollIndicator={false}>
              <View style={styles.formWrapper}>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    onChangeText={driver_name => this.setState({ driver_name })}
                    value={this.state.driver_name}
                    placeholder="Name" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    keyboardType={"number-pad"}
                    onChangeText={driver_phone => this.setState({ driver_phone })}
                    value={this.state.driver_phone}
                    placeholder="Mobile Number" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    onChangeText={driver_email => this.setState({ driver_email })}
                    value={this.state.driver_email}
                    autoCapitalize="none"
                    placeholder="Email" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    onChangeText={driver_pan_no => this.setState({ driver_pan_no })}
                    value={this.state.driver_pan_no}
                    placeholder="Pan Number" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    onChangeText={driver_aadhar_no => this.setState({ driver_aadhar_no })}
                    value={this.state.driver_aadhar_no}
                    placeholder="Adhar Number" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    onChangeText={driver_drive_licence_no => this.setState({ driver_drive_licence_no })}
                    value={this.state.driver_drive_licence_no}
                    placeholder="Driving Licence No." />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handleCountryPicker}
                    style={styles.picker}
                    items={this.CoutryData}
                    placeholder="Country"
                    placeholderStyle={{ color: '#6f7a8c' }}
                    item={this.state.driver_country}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                  <Image source={Images.black_dropdwn} style={styles.dropdown} />
                </View>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handleStatePicker}
                    style={styles.picker}
                    items={this.StateData}
                    placeholder="State"
                    placeholderStyle={{ color: '#6f7a8c' }}
                    item={this.state.driver_state}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                  <Image source={Images.black_dropdwn} style={styles.dropdown} />
                </View>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handleCityPicker}
                    style={styles.picker}
                    items={this.CityData}
                    placeholder="City"
                    placeholderStyle={{ color: '#6f7a8c' }}
                    item={this.state.driver_city}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                  <Image source={Images.black_dropdwn} style={styles.dropdown} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    onChangeText={driver_pincode => this.setState({ driver_pincode })}
                    value={this.state.driver_pincode}
                    placeholder="Pincode" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handleGenderPicker}
                    style={styles.picker}
                    items={this.GenderData}
                    placeholder="Gender"
                    placeholderStyle={{ color: '#6f7a8c' }}
                    item={this.state.driver_gender}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                  <Image source={Images.black_dropdwn} style={styles.dropdown} />
                </View>
                {/* <View style={styles.formBox}>
                  <TouchableOpacity style={{ width: '50%', backgroundColor: '#A0A0A0', marginLeft: 10, padding: 10, borderRadius: 10 }} onPress={this.selectIdProofImage}>
                    <Text>Id Proof Image</Text>
                  </TouchableOpacity>
                  {this.state.id_image != '' && (
                    <Image source={{ uri: this.state.id_image }} style={{ width: 50, height: 50, marginLeft: 50 }} />
                  )}
                </View> */}
                {/* <View style={styles.formBox}>
                  <TouchableOpacity style={{ width: '50%', backgroundColor: '#A0A0A0', marginLeft: 10, padding: 10, borderRadius: 10 }} onPress={this.selectPancardImage}>
                    <Text>Pan Card Image</Text>
                  </TouchableOpacity>
                  {this.state.pancard_image != '' && (
                    <Image source={{ uri: this.state.pancard_image }} style={{ width: 50, height: 50, marginLeft: 50 }} />
                  )}
                </View> */}
                {/* <View style={styles.formBox}>
                  <TouchableOpacity style={{ width: '50%', backgroundColor: '#A0A0A0', marginLeft: 10, padding: 10, borderRadius: 10 }} onPress={this.selectAadharImage}>
                    <Text>Aadhar Card Image</Text>
                  </TouchableOpacity>
                  {this.state.aadhar_image != '' && (
                    <Image source={{ uri: this.state.aadhar_image }} style={{ width: 50, height: 50, marginLeft: 50 }} />
                  )}
                </View> */}
                {/* <View style={styles.formBox}>
                  <TouchableOpacity style={{ width: '50%', backgroundColor: '#A0A0A0', marginLeft: 10, padding: 10, borderRadius: 10 }} onPress={this.selectDriverImage}>
                    <Text>Driver Image</Text>
                  </TouchableOpacity>
                  {this.state.driver_image != '' && (
                    <Image source={{ uri: this.state.driver_image }} style={{ width: 50, height: 50, marginLeft: 50 }} />
                  )}
                </View> */}
                <TouchableOpacity style={styles.submit} onPress={() => { this.onSubmit() }}>
                  <Text style={styles.submitText}>Submit</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    adddriver1: state.adddriver.adddriver
  };
};
export default connect(mapStateToProps, {
  adddriver
})(AddDriver);
