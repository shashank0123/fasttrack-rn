import axios from 'axios';
import urlObj from '../../url';
export const rsalist = () => {
  return async (dispatch, getState) => {

    url = urlObj.rsalist_url;
    await axios
      .get(url)
      .then((response) => {
        // console.log(response.data)
        dispatch({
          type: 'rsalist',
          payload: response.data.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};