import React, { Component, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  SafeAreaView,
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
// import {Picker} from '@react-native-picker/picker';
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { vehicle_list } from '../../redux/actions/vehicle_list';
import DatePicker from 'react-native-datepicker';
class HistoryReplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',
      // date: new Date(),
      from_time: new Date(),
      to_time: new Date(),
      show_start_time: false,
      show_end_time: false,
      show_date: false,
      date_placeholder: true,
      start_time_placeholder: true,
      end_time_placeholder: true,
      pickedVehicle: '',
      vehicle_list: [],
      pickedDate: null,
      date: ""
    }
    this._getStorageValue()
  }
  data = [
    { label: "Select Vehicle ", value: 1 },
  ];
  handlePicker = data => {
    this.setState({ pickedVehicle: data });
    console.log(data)
    this.setState({ vehicle_id: data.value })
  };
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.props.vehicle_list(value).then(() => {
        var vehicle_list = [{ label: "Select Vehicle ", value: 1 }]
        vehicle_list = this.props.vehicle_list1.map((obj) => {
          var obj1 = {
            label: obj.vehicle_no,
            value: obj.vehicle_id
          }
          return obj1
          // console.log(obj1)
        })
        this.data = vehicle_list
        if (this.data.length == 1) {
          this.setState({ pickedVehicle: this.data[0], vehicle_id: this.data[0].value })
        }
        this.setState({ vehicle_list: vehicle_list })
      })
    }
    return value
  }
  onChangeStartTime = (event, selectedStartTime) => {
    selectedStartTime = selectedStartTime;
    console.log(selectedStartTime)
    if (Platform.OS === 'ios')
      this.setState({ show_start_time: true })
    else
      this.setState({ show_start_time: false })
    this.setState({ from_time: selectedStartTime, start_time_placeholder: false });
    console.log(this.state.from_time)
  };
  onChangeEndTime = (event, selectedEndTime) => {
    selectedEndTime = selectedEndTime;
    console.log(selectedEndTime)
    if (Platform.OS === 'ios')
      this.setState({ show_end_time: true })
    else
      this.setState({ show_end_time: false })
    this.setState({ to_time: selectedEndTime, end_time_placeholder: false });
    console.log(this.state.to_time)
  };
  formatDate = (date) => {
    return `${date.getFullYear()}/${date.getMonth() +
      1}/${date.getDate()}`;
  };
  formatValue(item) {
    item = '00' + item;
    return item.slice(-2)
  }
  formatTime = (time) => {
    return time
    console.log(time)
    return `${this.formatValue(time.getHours())}:${this.formatValue(time.getMinutes())}`;
  };
  showDatepicker = () => {
    this.setState({ show_date: true });
  };
  showStartTimepicker = () => {
    this.setState({ show_start_time: true });
  };
  showEndTimepicker = () => {
    this.setState({ show_end_time: true });
  };
  submitButton() {
    this.props.navigation.navigate('HistoryReplayDetails', { vehicle_id: this.state.vehicle_id, date: this.state.date, from_time: this.formatTime(this.state.from_time), to_time: this.formatTime(this.state.end_time) })
  }
  render() {
    // console.log(this.props.vehicle_list1)
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>History Replay</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.pickerBox}>
              <Image source={Images.car_white} style={styles.carIcon} />
              <Picker
                onItemChange={this.handlePicker}
                style={styles.picker}
                items={this.data}
                placeholder="All Vehicles"
                placeholderStyle={{ color: '#fff' }}
                item={this.state.pickedVehicle}
              // backdropAnimation={{ opactity: 0 }}
              //androidPickerMode="dropdown"
              //isNullable
              //disable
              />
              <Image source={Images.white_dropdown} style={styles.dropdown} />
            </View>
            <View style={styles.formBox}>
              <Image source={Images.date_calendar} style={styles.searchIcon} />
              <DatePicker
                style={{ width: 270, }}
                date={this.state.date}
                mode="date"
                showIcon={false}
                placeholder={(this.state.date_placeholder) ? ("Date") : (this.formatDate(this.state.date))}
                format="DD-MM-YYYY"
                minDate="01-01-2006"
                maxDate="01-01-2099"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateInput: {
                    height: 50,
                    borderWidth: 0,
                    alignItems: 'flex-start',
                    marginLeft: 5
                  },
                  placeholderText: {
                    fontSize: 18,
                    color: '#6f7a8c',
                  },
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(date) => { this.setState({ date: date }) }}
              />
            </View>
            <View style={styles.formBox}>
              <Image source={Images.time_clock} style={styles.searchIcon} />
              <TouchableOpacity style={styles.formshortInput}
                onPress={this.showStartTimepicker}>
                <Text style={styles.placeholderText}>{(this.state.start_time_placeholder) ? ("Start Time") : (this.formatTime(this.state.from_time))}</Text>
                {this.state.show_start_time && (
                  <DatePicker
                    style={{ width: 270, }}
                    date={this.state.from_time}
                    mode="time"
                    showIcon={false}
                    placeholder={(this.state.start_time_placeholder) ? ("Date") : (this.formatDate(this.state.from_time))}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      // dateIcon: {
                      //   position: 'absolute',
                      //   left: 0,
                      //   top: 4,
                      //   marginLeft: 0
                      // },
                      dateInput: {
                        height: 50,
                        borderWidth: 0,
                        alignItems: 'flex-start',
                        marginLeft: 5
                      },
                      placeholderText: {
                        fontSize: 18,
                        color: '#6f7a8c',
                      },
                    }}
                    onDateChange={(date) => { this.setState({ from_time: date }) }}
                    is24Hour={true}
                  />
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.formBox}>
              <Image source={Images.time_clock} style={styles.searchIcon} />
              <TouchableOpacity style={styles.formshortInput}
                onPress={this.showEndTimepicker}>
                <Text style={styles.placeholderText}>{(this.state.end_time_placeholder) ? ("End Time") : (this.formatTime(this.state.to_time))}</Text>
                {this.state.show_end_time && (
                  <DatePicker
                    style={{ width: 270, }}
                    date={this.state.end_time}
                    mode="time"
                    showIcon={false}
                    placeholder={(this.state.end_time_placeholder) ? ("Date") : (this.formatDate(this.state.to_time))}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateInput: {
                        height: 50,
                        borderWidth: 0,
                        alignItems: 'flex-start',
                        marginLeft: 5
                      },
                      placeholderText: {
                        fontSize: 18,
                        color: '#6f7a8c',
                      },
                    }}
                    onDateChange={(date) => { this.setState({ end_time: date }) }}

                    is24Hour={true}
                  />
                )}
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.submit} onPress={() => { this.submitButton() }}>
              <Text style={styles.submitText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    vehicle_list1: state.vehicle_list.vehicle_list
  };
};
export default connect(mapStateToProps, {
  vehicle_list
})(HistoryReplay);
