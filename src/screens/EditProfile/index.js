import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,

  TextInput,
  Dimensions,
  Image,
  SafeAreaView,

} from "react-native";
import DatePicker from 'react-native-datepicker';

import styles from './style';
import { Images } from '../../utils';

import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { profile } from '../../redux/actions/profile';
import { saveprofile } from '../../redux/actions/saveprofile';
import { Picker } from 'react-native-woodpicker';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',

      show_date: false,
      date_placeholder: true,
      changeNameHeader: false,
      changeNumberHeader: false,
      changeMailHeader: false,
      pickedData: '',
      name: '',
      email: '',
      dob: new Date(),
      gender: '',
      mobile: '',
      avatarSource: '',
      drawer_profile: {},
      token: '',
      profile_pic: '',
      pickedGender: ''
    }
    this._getStorageValue()
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })
      this.props.profile(value).then(() => {
        console.log(this.props.profile1)
        this.setState({
          profile: this.props.profile1,

          name: this.props.profile1.name,
          gender: this.props.profile1.gender,
          mobile: this.props.profile1.mobile,
          // dob: this.props.profile1.dob,
          email: this.props.profile1.email,
          avatarSource: this.props.profile1.profile_pic,

        });
      })
    }
    return value
  }
  showDatepicker = () => {
    this.setState({ show_date: true });
  };

  formatDate = (date) => {
    return `${date.getDate()}/${date.getMonth() +
      1}/${date.getFullYear()}`;
  };





  onChangeDate = (event, selectedDate) => {
    selectedDate = selectedDate;
    if (Platform.OS === 'ios')
      this.setState({ show_date: true })
    else
      this.setState({ show_date: false })
    this.setState({ dob: selectedDate, date_placeholder: false });
    console.log(this.state.dob)
  };





  changeNameHeader() {
    this.setState({ changeNameHeader: !(this.state.changeNameHeader) })
  }
  changeNumberHeader() {
    this.setState({ changeNumberHeader: !(this.state.changeNumberHeader) })
  }
  changeMailHeader() {
    this.setState({ changeMailHeader: !(this.state.changeMailHeader) })
  }



  data = [
    { label: "Select Gender ", value: '' },
    { label: "Male", value: 'male' },
    { label: "Female", value: 'female' },
    { label: "Other", value: 'others' },
  ];


  handlePicker = data => {
    this.setState({ pickedGender: data });
    console.log(this.state.gender)
    this.setState({ gender: data.value })

  };

  onSave() {
    // console.log(this.state)
    this.props.saveprofile(this.state).then(async () => {
      console.log(this.props.saveprofile1)
      // this.props.navigation.replace('DashboardMap')
    })
    // this.showAlert();
  }

  selectImage = async () => {
    let options = {
      title: 'You can choose one image',
      maxWidth: 256,
      maxHeight: 256,
      quality: 0.5,
      includeBase64: true,
      storageOptions: {
        skipBackup: true
      }
    };

    launchImageLibrary(options, (response) => {
      this.setState({ drawer_profile: response.base64 })
      // response.token = this.state.token;
      // this.props.saveprofile(response);
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          avatarSource: response.uri.replace('content://', ''),
        });
      }
    });
  }



  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Edit Profile</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>

            <TouchableOpacity onPress={this.selectImage}>
              <View style={styles.profileImgWrapper}>
                {/* <Image source={Images.drawer_profile} style={{ height: 90, width: 90, borderRadius: 50, marginTop:20, }} /> */}
                <Image source={this.props.profile1.profile_pic ? { uri: this.props.profile1.profile_pic } : Images.drawer_profile} style={{ height: 90, width: 90, borderRadius: 50, marginTop: 20 }} />
                {/* <Image source={Images.edit_pic} style={styles.editPic} /> */}
              </View>
            </TouchableOpacity>
            <View style={styles.listWrapper}>
              <View style={styles.formBox}>
                <Text style={(this.state.changeNameHeader) ? (styles.fieldText2) : (styles.fieldText)}>Name</Text>
                <Image source={Images.person_Icon} style={styles.searchIcon} />
                <TextInput style={styles.formshortInput}
                  placeholderTextColor="#000000"
                  onChangeText={name => this.setState({ name })}
                  value={this.state.name}
                  onFocus={() => { this.changeNameHeader() }}
                  onEndEditing={() => { this.changeNameHeader() }}
                  placeholder="" />
              </View>


              <View style={styles.formBox}>
                <Image source={Images.call_Icon} style={styles.searchIcon} />
                <Text style={(this.state.changeNumberHeader) ? (styles.fieldText2) : (styles.fieldText)}>Mobile Number</Text>
                <TextInput style={styles.formshortInput}
                  placeholderTextColor="#000000"
                  onChangeText={mobile => this.setState({ mobile })}
                  value={this.state.mobile}
                  onFocus={() => { this.changeNumberHeader() }}
                  onEndEditing={() => { this.changeNumberHeader() }}
                  keyboardType='number-pad'
                  maxLength={12}
                  placeholder="" />
              </View>


              <View style={styles.formBox}>
                <Image source={Images.email} style={styles.searchIcon} />
                <Text style={(this.state.changeMailHeader) ? (styles.fieldText2) : (styles.fieldText)}>Email or Username</Text>
                <TextInput style={styles.formshortInput}
                  placeholderTextColor="#000000"
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email}
                  onFocus={() => { this.changeMailHeader() }}
                  onEndEditing={() => { this.changeMailHeader() }}
                  placeholder="test83" />
              </View>




              <View style={styles.formRow}>



                <TouchableOpacity style={styles.formShortBox}
                  onPress={this.showDatepicker}>
                  <Image source={Images.date_calendar} style={styles.searchIcon} />
                  <Text style={styles.placeholderText}>{(this.state.date_placeholder) ? ("Date of Birth") : (this.formatDate(this.state.dob))}</Text>
                  {this.state.show_date && (
                    <DatePicker
                      style={styles.formshortInput}
                      date={this.state.dob}
                      mode="date"
                      showIcon={false}
                      format="DD-MM-YYYY"
                      minDate="01-01-2006"
                      maxDate="01-01-2099"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          height: 50,
                          borderWidth: 0,
                          alignItems: 'flex-start',
                          marginLeft: 5
                        },
                        placeholderText: {
                          fontSize: 18,
                          color: '#6f7a8c',
                        },
                        // ... You can check the source to find the other keys.
                      }}
                      onDateChange={(dob) => { this.setState({ dob: dob }) }}
                    />
                  )}
                </TouchableOpacity>





                <View style={styles.formShortBox2}>
                  <Picker
                    onItemChange={this.handlePicker}
                    style={styles.picker}
                    items={this.data}
                    placeholder="Select Gender"
                    placeholderStyle={{ color: '#000' }}
                    item={this.state.pickedGender}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                </View>
              </View>
              <TouchableOpacity style={styles.submit} onPress={() => { this.onSave() }}>
                <Text style={styles.submitText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    profile1: state.profile.profile,
    saveprofile1: state.saveprofile.saveprofile
  };
};
export default connect(mapStateToProps, {
  profile, saveprofile
})(EditProfile);
