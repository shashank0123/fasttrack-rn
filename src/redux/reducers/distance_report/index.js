const initialState = {
    distance_report: {}
  };
  
  export const distance_report = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        // console.log(action.payload)
        return { ...state,distance_report:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default distance_report;