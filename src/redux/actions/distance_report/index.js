import axios from 'axios';
import urlObj from '../../url';
export const distance_report = (state) => {
  return async (dispatch, getState) => {
    
    url = urlObj.distance_report_url+"?token="+state.token+"&vehicle_id="+state.vehicle_id+"&from_date="+state.from_date+"&to_date="+state.to_date;
    console.log(url)
    await axios
      .get(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'LOGIN',
          payload: response.data.data
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};