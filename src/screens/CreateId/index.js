import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  SafeAreaView
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
import CheckBox from '@react-native-community/checkbox';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { sub_user, sub_user_status } from '../../redux/actions/sub_user';
import { delete_id } from '../../redux/actions/delete_id';


class CreateId extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sub_user: [],
      checked: []
    }
    this._getStorageValue()
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      this.setState({ token: value })
      this.props.sub_user(value).then(() => {
        this.data = sub_user
        this.props.sub_user1.forEach((number, index) => {
          if (number.account_status == 'active')
            this.state.checked[index] = true
          else
            this.state.checked[index] = false
        })
        this.setState({ sub_user: this.props.sub_user1 })
      })
    }
    return value
  }
  moveToSlideFromRight(item, index) {

    this.props.delete_id(this.state.token, item.id).then(() => {
      this.data = delete_id
      console.log(this.props.delete_id1)
      this._getStorageValue()
    })
  }

  changeUserStatus(item, index) {
    this.state.checked[index] = !this.state.checked[index]
    if (this.state.checked[index]) {
      var userstatus = 'active'
    }
    else
      var userstatus = 'deactive'
    this.props.sub_user_status(this.state.token, item.id, userstatus)
    console.log(this.state.checked)
    console.log(item)


  }

  render() {
    console.log(this.props.sub_user1)
    return (
      <SafeAreaView style={styles.container}>
        {/* <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}> */}


        <View style={styles.upperBar}>
          <View style={styles.backIconContainer}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={Images.back} style={styles.barMenuIcon} />
            </TouchableOpacity>
          </View>
          <View style={styles.headingContainer}>
            <Text style={styles.barText}>Sub User</Text>
          </View>
        </View>
        <View style={styles.flatlistWrapper}>
          <ScrollView
            contentContainerStyle={styles.scrollWrapper}
            showsVerticalScrollIndicator={false}>
            <FlatList
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }}
              keyExtractor={(item, index) => index.toString()}
              data={this.state.sub_user}
              renderItem={({ item, index }) =>


                <View key={index} style={styles.docDetailedWrapper2}>
                  <View
                    style={[styles.docSpecsWrapper, { marginTop: 10 }]}>
                    <View style={styles.docNameWrapper}>
                      <Text numberOfLines={1} style={styles.docNameText}>
                        Name: {item.name}
                      </Text>
                      <Text numberOfLines={2} style={styles.docSubNameText}>
                        Mobile Number: {item.mobile}
                      </Text>
                      <Text numberOfLines={2} style={styles.docSubNameText}>
                        Email: {item.email}
                      </Text>
                    </View>
                    <View style={{ marginLeft: -40, marginTop: 5 }}>
                      <CheckBox
                        style={{}}
                        value={this.state.checked[index]}
                        tintColors={{ true: '#006631', false: '#A0A0A0' }}
                        onValueChange={() => { this.changeUserStatus(item, index) }}
                      />

                    </View>
                    <View>
                      <TouchableOpacity
                        activeOpacity={1}
                        style={styles.boxBtn}
                        onPress={() => this.moveToSlideFromRight(item, index)} >
                        <Text style={styles.btnText}>Delete</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>


              } />
          </ScrollView>
        </View>

        <TouchableOpacity style={styles.bottomBar} onPress={() => { this.props.navigation.replace("AddSubUser"); }}>
          <View style={styles.bottomContainer}>
            <Text style={styles.barText}> Add Sub User</Text>
          </View>

        </TouchableOpacity>



      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    sub_user1: state.sub_user.sub_user,
    delete_id1: state.delete_id.delete_id,
    sub_user_status1: state.sub_user.sub_user_status
  };
};
export default connect(mapStateToProps, {
  sub_user, delete_id, sub_user_status
})(CreateId);

