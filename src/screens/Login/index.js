import React, { Component, setState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Keyboard,
  Image,
  ImageBackground, Alert
} from "react-native";
import { connect } from 'react-redux';
import Tts from 'react-native-tts';
import styles from './style';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';
import { login } from '../../redux/actions/login';
import { Images } from '../../utils';
import { fcmService } from './../../utils/FCMService';
import LoadingIcon from '../../components/LoadingIcon';
class Login extends React.Component {
  state = { loadingApp: false }
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      email: '', password: false, device_token: "", imei: "",
      mobile_os: ''
    }
    fcmService.registerAppWithFCM()
    // fcmService.register(onRegister, onNotification, onOpenNotification)
    this._getStorageValue();
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    var fcm = await AsyncStorage.getItem('fcmToken')
    var mobile_os = await AsyncStorage.getItem('mobile_os')
    this.state.device_token = fcm
    this.state.mobile_os = mobile_os
    if (value != null) {
      console.log(value)
      this.props.navigation.replace('Home')
    }
    else {
      await AsyncStorage.removeItem('token')
    }
    return value
  }
  loadingRender() {
    if (this.state.loadingApp) {
      return (
        <LoadingIcon />
      )
    }
  }
  // state = {  }
  changeMobileNumber(mobile) {
    this.setState({ mobile })
    if (this.state.mobile.length >= 10) {
      Keyboard.dismiss();
    }
  }
  async submitLogin() {
    //this._getStorageValue();
    messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          this.state.device_token = fcmToken
          console.log(this.state.device_token)
        }
      })
      .catch(error => {
        console.log("[FCMService] getToken rejected", error)
      })
    if (this.state.device_token != null) {
      this.setState({ loadingApp: true })
      this.props.login(this.state).then(async () => {
        this.setState({ loadingApp: false })
        var value = await AsyncStorage.getItem('token')
        if (value != null) {
          console.log(value)
          Tts.speak('Welcome to Fasttrack');
          this.props.navigation.replace('Home')
        }
        else {
          console.log(this.props.login1, 'shashank234')
          Alert.alert(
            'Fastrack',
            this.props.login1.error
          )
        }
      })
    }
    // this.setState({ loadingApp: false })
  }
  render() {
    return (
      <ImageBackground source={Images.bg_map} resizeMode="cover" style={styles.imgBg}>
        <View style={styles.container}>
          <View style={styles.wrapper}>
            {this.loadingRender()}
            <View style={styles.formWrapper}>
              <View style={styles.loginImageWrapper}>
                <Image source={Images.logo_new2} style={styles.logo} />
                <Text style={styles.loginText}>Login</Text>
              </View>
              <View style={styles.formBox}>
                <TextInput style={styles.formshortInput}
                  placeholderTextColor="#6f7a8c"
                  underlineColorAndroid="transparent"
                  onChangeText={email => this.setState({ email })}
                  value={this.state.email}
                  autoCapitalize="none"
                  placeholder="Username" />
                <Image source={Images.person_Icon} style={styles.searchIcon} />
              </View>
              <View style={styles.formBox}>
                <TextInput style={styles.formshortInput}
                  placeholderTextColor="#6f7a8c"
                  onChangeText={password => this.setState({ password })}
                  placeholder="Password"
                  secureTextEntry={this.state.hidePassword} />
                <TouchableOpacity onPress={() => { this.setState({ hidePassword: !(this.state.hidePassword) }) }}>
                  <Image source={(this.state.hidePassword) ? (Images.eye_line) : (Images.eye)} style={styles.searchIcon} />
                </TouchableOpacity>
              </View>
              <View style={styles.formBox}>
                <TextInput style={styles.formshortInput}
                  underlineColorAndroid="transparent"
                  placeholderTextColor="#6f7a8c"
                  maxLength={10}
                  dataDetectorTypes={'phoneNumber'}
                  enablesReturnKeyAutomatically={true}
                  keyboardType='number-pad'
                  // onChangeText={mobile => {this.changeMobileNumber()}}
                  onChangeText={mobile => {
                    this.setState({ mobile })
                    if (mobile.length > 9) {
                      Keyboard.dismiss()
                    }
                  }}
                  value={this.state.mobile}
                  onSubmitEditing={() => { this.submitLogin() }}
                  placeholder="Mobile Number" />
                <Image source={Images.call_Icon} style={styles.callIcon} />
              </View>
              <TouchableOpacity style={styles.submit} onPress={() => this.submitLogin()}>
                <Text style={styles.submitText}>Login</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    login1: state.login.login
  };
};
export default connect(mapStateToProps, {
  login
})(Login);
