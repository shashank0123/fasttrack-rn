import axios from 'axios';
import urlObj from '../../url';
export const registeration = (state) => {
  return async (dispatch, getState) => {

    url = urlObj.registeration_url + "?token=" + state.token;;
    var data = new FormData()
    data.append('device_no', state.device_no)
    data.append('sim_no', state.sim_no)
    data.append('vehicle_no', state.vehicle_no)
    data.append('vehicle_type', state.vehicle_type)
    data.append('user_name', state.user_name)
    data.append('mobile_no', state.mobile_no)
    console.log(data)
    await axios
      .post(url, data)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'MAINUSER',
          payload: response.data.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};