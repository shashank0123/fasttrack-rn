import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
// import {Picker} from '@react-native-picker/picker';
// import DateTimePicker from '@react-native-community/datetimepicker';
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { vehicle_list } from '../../redux/actions/vehicle_list';
import { trip_report } from '../../redux/actions/trip_report';
import DatePicker from 'react-native-datepicker';
class Report_Trip extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',
      show_start_date: false,
      start_date_placeholder: true,
      pickedVehicle: '',
      vehicle_list: [],
      start_date: '',
    }
    this._getStorageValue()
  }
  data = [
    { label: "Select Vehicle ", value: 1 },
  ];
  handlePicker = data => {
    this.setState({ pickedVehicle: data });
    console.log(data)
    this.setState({ vehicle_id: data.value })
  };
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.props.vehicle_list(value).then(() => {
        var vehicle_list = []
        vehicle_list = this.props.vehicle_list1.map((obj) => {
          var obj1 = {
            label: obj.vehicle_no,
            value: obj.vehicle_id
          }
          return obj1
          // console.log(obj1)
        })
        this.data = vehicle_list
        this.setState({ vehicle_list: this.props.vehicle_list1 })
      })
    }
    return value
  }
  formatDate = (date) => {
    return `${date.getDate()}/${date.getMonth() +
      1}/${date.getFullYear()}`;
  };
  showStartDatepicker = () => {
    this.setState({ show_start_date: true });
  };
  onChangeStartDate = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    if (Platform.OS === 'ios')
      this.setState({ show_start_date: true })
    else
      this.setState({ show_start_date: false })
    this.setState({ start_date: currentDate, start_date_placeholder: false });
  };
  submitButton() {
    this.props.navigation.navigate('TripInformation', { vehicle_id: this.state.vehicle_id, from_date: this.state.start_date })
  }
  render() {
    // console.log(this.props.vehicle_list1)
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Trip Report</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.pickerBox}>
              <Image source={Images.car_white} style={styles.carIcon} />
              <Picker
                onItemChange={this.handlePicker}
                style={styles.picker}
                items={this.data}
                placeholder="All Vehicles"
                placeholderStyle={{ color: '#fff' }}
                item={this.state.pickedVehicle}
              // backdropAnimation={{ opactity: 0 }}
              //androidPickerMode="dropdown"
              //isNullable
              //disable
              />
              <Image source={Images.white_dropdown} style={styles.dropdown} />
            </View>
            <View style={styles.formBox}>
              <Image source={Images.date_calendar} style={styles.searchIcon} />
              <DatePicker
                style={{ width: 270, }}
                date={this.state.start_date}
                mode="date"
                showIcon={false}
                placeholder={(this.state.start_date_placeholder) ? ("Start Date") : (this.formatDate(this.state.start_date))}
                format="DD-MM-YYYY"
                minDate="01-01-2006"
                maxDate="01-01-2099"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  // dateIcon: {
                  //   position: 'absolute',
                  //   left: 0,
                  //   top: 4,
                  //   marginLeft: 0
                  // },
                  dateInput: {
                    height: 50,
                    borderWidth: 0,
                    alignItems: 'flex-start',
                    marginLeft: 5
                  },
                  placeholderText: {
                    fontSize: 18,
                    color: '#6f7a8c',
                  },
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(start_date) => { this.setState({ start_date: start_date }) }}
              />
            </View>
            <TouchableOpacity style={styles.submit} onPress={() => { this.submitButton() }}>
              <Text style={styles.submitText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    vehicle_list1: state.vehicle_list.vehicle_list,
    trip_report1: state.trip_report.trip_report
  };
};
export default connect(mapStateToProps, {
  vehicle_list, trip_report
})(Report_Trip);
