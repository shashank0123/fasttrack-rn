import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Button, SafeAreaView
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
class NewRegistration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      selectedSim: '',
      pickedSim: '',
      selectedDevice: '',
      pickedDevice: '',
      selectedBrand: '',
      pickedVehBrand: '',
      selectedVechType: '',
      pickedVehType: '',
      image: ''
    }
  }
  SimData = [
    { label: "Select SIM ", value: '' },
    { label: "Vodafone", value: 1 },
    { label: "Airtel", value: 2 },
  ];
  handleSimPicker = data => {
    this.setState({ pickedSim: data });
  };
  DeviceData = [
    { label: "Select Device ", value: '' },
    { label: "Vodafone", value: 1 },
    { label: "Airtel", value: 2 },
  ];
  handleDevicePicker = data => {
    this.setState({ pickedDevice: data });
  };
  VehBrandData = [
    { label: "Select Brand ", value: '' },
    { label: "Vodafone", value: 1 },
    { label: "Airtel", value: 2 },
  ];
  handleVehBrandPicker = data => {
    this.setState({ pickedVehBrand: data });
  };
  VehTypeData = [
    { label: "Select Brand ", value: '' },
    { label: "Vodafone", value: 1 },
    { label: "Airtel", value: 2 },
  ];
  handleVehTypePicker = data => {
    this.setState({ pickedVehType: data });
  };
  selectVechImage = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      console.log('Image is here', image);
      this.setState({ veh_image: image.path })
    });
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>New Registration</Text>
            </View>
          </View>
          <KeyboardAvoidingView behavior="padding">
            <ScrollView
              contentContainerStyle={styles.scrollWrapper}
              showsVerticalScrollIndicator={false}>
              <View style={styles.formWrapper}>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>Device</Text>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    onChangeText={(device_number) => { this.setState({ device_number }) }}
                    autoCapitalize="none"
                    placeholder="Device Number" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    placeholder="SIM Number" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    placeholder="Activation Date" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    placeholder="Expiry Date" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    placeholder="Price Of Device" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handleSimPicker}
                    style={styles.picker}
                    items={this.SimData}
                    placeholder="SIM Type"
                    placeholderStyle={{ color: '#6f7a8c' }}
                    item={this.state.pickedSim}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                  <Image source={Images.black_dropdwn} style={styles.dropdown} />
                </View>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handleDevicePicker}
                    style={styles.picker}
                    items={this.DeviceData}
                    placeholder="Device Type"
                    placeholderStyle={{ color: '#6f7a8c' }}
                    item={this.state.pickedDevice}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                  <Image source={Images.black_dropdwn} style={styles.dropdown} />
                </View>
                <Text style={{ fontSize: 18, fontWeight: 'bold', marginTop: 20 }}>Vehicle</Text>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handleVehBrandPicker}
                    style={styles.picker}
                    items={this.VehBrandData}
                    placeholder="Vehicle Brand"
                    placeholderStyle={{ color: '#6f7a8c' }}
                    item={this.state.pickedVehBrand}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                  <Image source={Images.black_dropdwn} style={styles.dropdown} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    placeholder="Vehicle Number" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    placeholder="Vehicle Name" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TouchableOpacity style={{ width: '50%', backgroundColor: '#A0A0A0', marginLeft: 10, padding: 10, borderRadius: 10 }} onPress={this.selectVechImage}>
                    <Text>Choose Vehicle Image</Text>
                  </TouchableOpacity>
                  {this.state.veh_image != '' && (
                    <Image source={{ uri: this.state.veh_image }} style={{ width: 50, height: 50, marginLeft: 50 }} />
                  )}
                </View>
                <View style={styles.pickerBox}>
                  <Picker
                    onItemChange={this.handleVehTypePicker}
                    style={styles.picker}
                    items={this.VehTypeData}
                    placeholder="Vehicle Type"
                    placeholderStyle={{ color: '#6f7a8c' }}
                    item={this.state.pickedVehType}
                  // backdropAnimation={{ opactity: 0 }}
                  //androidPickerMode="dropdown"
                  //isNullable
                  //disable
                  />
                  <Image source={Images.black_dropdwn} style={styles.dropdown} />
                </View>
                <Text style={{ fontSize: 18, fontWeight: 'bold', marginTop: 20 }}>User</Text>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    placeholder="Owner Name" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    secureTextEntry={true}
                    placeholder="Password" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    autoCapitalize="none"
                    keyboardType={"number-pad"}
                    placeholder="Mobile Number" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <TouchableOpacity style={styles.submit} onPress={() => { }}>
                  <Text style={styles.submitText}>Add</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
  };
};
export default connect(mapStateToProps, {
})(NewRegistration);
