import axios from 'axios';
import urlObj from '../../url';
export const setSpeed = (state) => {
  return async (dispatch, getState) => {
    data = {
      token: state.token,
      vehicle_id: state.vehicle_id,
      speed_max: state.speed
    }
    url = urlObj.setSpeed_url+'?token='+state.token+'&vehicle_id='+state.vehicle_id+'&speed_max='+state.speed;
    console.log(url)
    await axios
      .get(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'LOGIN',
          payload: response.data.data
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};