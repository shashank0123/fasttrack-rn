import axios from 'axios';
import urlObj from '../../url';
export const historyreplay = (state) => {
  return async (dispatch, getState) => {

    url = urlObj.history_replay_url + '?token=' + state.token + '&vehicle_id=' + state.vehicle_id +'&date=' + state.date  +'&from_time=' + state.from_time +'&to_time=' + state.to_time; 
    console.log(url)


    await axios
      .get(url)
      .then((response) => {
        console.log(response.data)
        dispatch({
          type: 'HISTORYREPLAY',
          payload: response.data

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};