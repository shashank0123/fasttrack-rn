var base_url = 'http://fasttrackgpsinfo.com/api'
const urlObj = {

  login_url: base_url + '/login',
  distance_report_url: base_url + '/distance_report',
  profile_url: base_url + '/profile_info',
  save_profile_url: base_url + '/update_info',
  vehicle_list_url: base_url + "/show_vehicles",
  history_replay_url: base_url + "/history_replay",
  trip_report_url: base_url + "/v2/trip_report",
  engine_on_url: base_url + "/engine_on",
  engine_off_url: base_url + "/engine_off",
  parking_status_url: base_url + "/car_parking",
  setSpeed_url: base_url + "/speed_limit",
  sub_user_url: base_url + "/show_sub_users",
  sub_user_status_url: base_url + "/sub_user_status",
  all_activity_url: base_url + "/car_activity",
  complaint_url: base_url + "/complaint",
  delete_id_url: base_url + "/delete_sub_user",
  create_id_url: base_url + "/add_sub_user",
  rsalist_url: base_url + "/rsalist",
  registeration_url: base_url + "/new_user_register",
  vehicle_types_url: base_url + "/vehicle_types",

  geofencing_url: base_url + "/user_geofencing",
  history_replay_url: base_url + "/history_replay",
  change_password_url: base_url + "/change_password",
  add_driver_url: base_url + "/add_driver",

};
module.exports = urlObj;
