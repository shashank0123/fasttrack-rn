import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    TextInput,
    Dimensions,
    Image,
    FlatList,
    Button,
    SafeAreaView,
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
class TripDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.wrapper}>
                    <View style={styles.upperBar}>
                        <View style={styles.backIconContainer}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                <Image source={Images.back} style={styles.barMenuIcon} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.headingContainer}>
                            <Text style={styles.barText}>Trip Details</Text>
                        </View>
                    </View>
                    <KeyboardAvoidingView behavior="padding">
                        <ScrollView
                            contentContainerStyle={styles.scrollWrapper}
                            showsVerticalScrollIndicator={false}>
                            <View>
                                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={Images.refresh_blue} style={styles.barMenuIcon} />
                                    <Text style={{ marginLeft: 20 }}>Trip Started</Text>
                                </View>
                                <View style={styles.formWrapper}>
                                </View>
                            </View>
                            <View>
                                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={Images.refresh_blue} style={styles.barMenuIcon} />
                                    <Text style={{ marginLeft: 20 }}>Trip Started</Text>
                                </View>
                                <View style={styles.formWrapper}>
                                </View>
                            </View>
                            <View>
                                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={Images.refresh_blue} style={styles.barMenuIcon} />
                                    <Text style={{ marginLeft: 20 }}>Trip Started</Text>
                                </View>
                                <View style={styles.formWrapper}>
                                </View>
                            </View>
                            <View>
                                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={Images.refresh_blue} style={styles.barMenuIcon} />
                                    <Text style={{ marginLeft: 20 }}>Trip Started</Text>
                                </View>
                                <View style={styles.formWrapper}>
                                </View>
                            </View>
                            <View>
                                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={Images.refresh_blue} style={styles.barMenuIcon} />
                                    <Text style={{ marginLeft: 20 }}>Trip Started</Text>
                                </View>
                                <View style={styles.formWrapper}>
                                </View>
                            </View>
                            <View>
                                <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }}>
                                    <Image source={Images.refresh_blue} style={styles.barMenuIcon} />
                                    <Text style={{ marginLeft: 20 }}>Trip Complete</Text>
                                </View>
                            </View>
                        </ScrollView>
                    </KeyboardAvoidingView>
                </View>
            </SafeAreaView>
        );
    }
}
export default TripDetails;
