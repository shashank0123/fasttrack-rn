import axios from 'axios';
import urlObj from '../../url';
export const saveprofile = (state) => {
  return async (dispatch, getState) => {
    
    url = urlObj.save_profile_url+"?token="+state.token+ "&name=" + state.name + "&email=" + state.email+ "&mobile=" + state.mobile + "&dob=" + state.dob+ "&gender=" + state.gender ;
    await axios
      .post(url)
      .then((response) => {
        dispatch({
          type: 'LOGIN',
          payload: response.data
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};