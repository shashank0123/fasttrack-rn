import axios from 'axios';
import urlObj from '../../url';
import AsyncStorage from '@react-native-async-storage/async-storage';
export const login = (login) => {
  return async (dispatch, getState) => {

    var url = urlObj.login_url + "?email=" + login.email + "&password=" + login.password + "&device_token=" + login.device_token + "&mobile=" + login.mobile + "&imei=" + login.imei + "&mobile_os=" + login.mobile_os;
    console.log(url)
    await axios
      .post(url)
      .then(async (response) => {
        console.log(response)

        if (response.data.data) {
          console.log('hi this is new')
          await AsyncStorage.setItem('token', response.data.data.token);
          await AsyncStorage.setItem('name', response.data.data.user.name);
          await AsyncStorage.setItem('phone', response.data.data.user.mobile);
          dispatch({
            type: 'LOGIN',
            payload: response.data.data

          });
        }

        // console.log(response);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          // Handle 400
          console.log(error.response)
          dispatch({
            type: 'LOGIN',
            payload: error.response.data

          });
        } else {
          // Handle else
        }
        console.log(error);
      });
  };
};