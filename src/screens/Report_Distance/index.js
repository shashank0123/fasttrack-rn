import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  SafeAreaView,
  Dimensions,
  Image,
  FlatList,
  Button,
} from "react-native";
import styles from './style';
import { Images } from '../../utils';
const { width, height } = Dimensions.get('window');
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { vehicle_list } from '../../redux/actions/vehicle_list';
import { distance_report } from '../../redux/actions/distance_report';
import DatePicker from 'react-native-datepicker';
const vehicles = [
];
class Report_Distance extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '',
      // start_date: new Date(),
      // end_date: new Date(),
      show_start_date: false,
      show_end_date: false,
      start_date_placeholder: true,
      end_date_placeholder: true,
      pickedVehicle: '',
      vehicle_list: [],
      token: '',
      vehicle_id: '',
      start_date: '',
      end_date: '',
      from_date: '',
      to_date: '',
      showButton: true,
    }
    this._getStorageValue()
  }
  data = [
    { label: "Select Vehicle ", value: 1 },
  ];
  handlePicker = data => {
    this.setState({ pickedVehicle: data });
    console.log(data)
    this.setState({ vehicle_id: data.value })
  };
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })
      this.props.vehicle_list(value).then(() => {
        var vehicle_list = []
        vehicle_list = this.props.vehicle_list1.map((obj) => {
          var obj1 = {
            label: obj.vehicle_no,
            value: obj.vehicle_id
          }
          return obj1
          // console.log(obj1)
        })
        this.setState({ vehicle_id: this.props.vehicle_list1[0].vehicle_id })
        this.data = vehicle_list
        this.setState({ vehicle_list: this.props.vehicle_list1 })
      })
    }
    return value
  }
  formatDate = (date) => {
    return `${date.getFullYear()}-${date.getMonth() +
      1}-${date.getDate()}`;
  };
  fixDate = (date) => {
    var newdate = date.split('-')
    return `${newdate[2]}-${newdate[1]}-${newdate[0]}`;
  };
  showStartDatepicker = () => {
    this.setState({ show_start_date: true });
  };
  showEndDatepicker = () => {
    this.setState({ show_end_date: true });
  };
  onChangeStartDate = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.start_date;
    if (Platform.OS === 'ios')
      this.setState({ show_start_date: true })
    else
      this.setState({ show_start_date: false })
    console.log(this.formatDate(currentDate));
    this.setState({ start_date: currentDate, from_date: this.formatDate(currentDate), start_date_placeholder: false });
  };
  onChangeEndDate = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.end_date;
    if (Platform.OS === 'ios')
      this.setState({ show_end_date: true })
    else
      this.setState({ show_end_date: false })
    this.setState({ end_date: currentDate, to_date: this.formatDate(currentDate), end_date_placeholder: false });
  };
  fetchData() {
    this.setState({ showButton: false })
    this.props.distance_report(this.state).then(response => {
      console.log(this.props.distance_report1)
      this.setState({ showButton: true })
    })
    // this.props.navigation.navigate('HistoryReportDetail')
  }
  renderSubmit() {
    if (this.state.showButton)
      return (<TouchableOpacity style={styles.submit} onPress={() => { this.fetchData() }}>
        <Text style={styles.submitText}>Submit</Text>
      </TouchableOpacity>);
    else
      return (<TouchableOpacity style={styles.submit}>
        <Text style={styles.submitText}>Fetching...</Text>
      </TouchableOpacity>)
  }
  renderReport() {
    if (this.props.distance_report1.length > 0)
      return (<><View style={styles.upperBar1}>
        <View style={styles.headingContainer}>
          <Text style={styles.barText}>Download Report</Text>
        </View>
      </View>
        <SafeAreaView style={{ flex: 1, position: 'relative', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', marginLeft: 10, marginRight: 10, }}>
            <FlatList
              // contentContainerStyle={{marginBottom:40}}
              showsVerticalScrollIndicator={true}
              data={this.props.distance_report1}
              renderItem={({ item }) => (
                <>
                  <View style={styles.formWrapper1}>
                    <View style={styles.formBox1}>
                      <Image
                        source={Images.date_calendar}
                        style={styles.searchIcon}
                      />
                      <Text>{item.date}</Text>
                    </View>
                    <View style={styles.formBox1}>
                      <Image
                        source={Images.date_calendar}
                        style={styles.searchIcon}
                      />
                      <Text>{item.distance}</Text>
                    </View>
                  </View>
                </>
              )}
              keyExtractor={(item) => item.id}
            />
          </View>
        </SafeAreaView></>)
  }
  render() {
    console.log(this.props.vehicle_list1)
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Distance Report</Text>
            </View>
          </View>
          <View style={styles.formWrapper}>
            <View style={styles.pickerBox}>
              <Image source={Images.car_white} style={styles.carIcon} />
              <Picker
                onItemChange={this.handlePicker}
                style={styles.picker}
                items={this.data}
                placeholder="All Vehicles"
                placeholderStyle={{ color: '#fff' }}
                item={this.state.pickedVehicle}
              />
              <Image source={Images.white_dropdown} style={styles.dropdown} />
            </View>
            <View style={styles.formBox}>
              <Image source={Images.date_calendar} style={styles.searchIcon} />
              <DatePicker
                style={{ width: 270, }}
                date={this.state.start_date}
                mode="date"
                showIcon={false}
                placeholder={(this.state.start_date_placeholder) ? ("Start Date") : (this.formatDate(this.state.start_date))}
                format="DD-MM-YYYY"
                minDate="01-01-2018"
                maxDate="01-01-2099"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateInput: {
                    height: 50,
                    borderWidth: 0,
                    alignItems: 'flex-start',
                    marginLeft: 5
                  },
                  placeholderText: {
                    fontSize: 18,
                    color: '#6f7a8c',
                  },
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(date) => { this.setState({ start_date: date, from_date: this.fixDate(date) }) }}
              />
            </View>
            <View style={styles.formBox}>
              <Image source={Images.date_calendar} style={styles.searchIcon} />
              <DatePicker
                style={{ width: 270, }}
                date={this.state.end_date}
                mode="date"
                showIcon={false}
                placeholder={(this.state.end_date_placeholder) ? ("End Date") : (this.formatDate(this.state.end_date))}
                format="DD-MM-YYYY"
                minDate="01-01-2006"
                maxDate="01-01-2099"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateInput: {
                    height: 50,
                    borderWidth: 0,
                    alignItems: 'flex-start',
                    marginLeft: 5
                  },
                  placeholderText: {
                    fontSize: 18,
                    color: '#6f7a8c',
                  },
                }}
                onDateChange={(date) => { this.setState({ end_date: date, to_date: this.fixDate(date) }) }}
              />
            </View>
            {this.renderSubmit()}
            {this.renderReport()}
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    vehicle_list1: state.vehicle_list.vehicle_list,
    distance_report1: state.distance_report.distance_report
  };
};
export default connect(mapStateToProps, {
  vehicle_list, distance_report
})(Report_Distance);
