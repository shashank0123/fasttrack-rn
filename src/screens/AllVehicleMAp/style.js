import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: 20,
    backgroundColor: "transparent"
  },

  image: {
    width: 30,
    height: 30
  },
  image_minus: {
    width: 30,
    height: 5
  },
  image_button: {
    marginTop: 10,
    width: 40,
    height: 40,
    alignSelf: "center"

  },
  tip: {
    position: 'relative',
    top: 20,
    backgroundColor: 'rgb(227,227,227)',
    zIndex: 1000,
    
    height: 100,
    marginRight: 7,
    flexDirection: 'column',
    alignItems: 'center'
  },

  maparea:{
    ...StyleSheet.absoluteFillObject, 
    marginTop:height*0.075,
    height:height*1,
    width : width,
    alignItems:'center',
    alignSelf:'center',
  },

  bottom_tab_container: {
    width: width,
    height: 150,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    bottom: -20,
    borderRadius: 35,
    shadowColor: 'black',
    shadowOpacity: 0.5,
    shadowOffset: {
      x: 5,
      y: 5
    },
  },

  bottom_btn: {
    position: 'relative', 
    top: 20, 
    backgroundColor: 'rgb(227,227,227)', 
    zIndex: 1000, 
    width: width / 5.5, 
    width: 85, 
    height: 100,  
    marginRight: 7, 
    flexDirection: 'column', 
    alignItems: 'center',    
  },

  bottom_btn_image_wrapper: {
    backgroundColor: 'white', 
    width: 80, 
    height: 60
  },

  bottom_btn_image_wrapper_stop: {
    backgroundColor: 'red', 
    width: 80, 
    height: 60
  },

  bottom_btn_text1: {
    fontSize: 10, color: 'black', paddingTop: 10
  },

  bottom_btn_text2: {
    fontSize: 10, color: 'black'
  },
  speed_text_wrapper: {
    width: 60,
    height: 60,
    backgroundColor: 'black',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 10,
    top: 140,
    borderRadius: 35,
    zIndex: 10000
  },

  speed_text: {
    fontSize: 25, 
    color: 'white'
  },

  speed_label: {
    fontSize: 15, 
    color: 'white'
  },

  addressbar_container: {
    width: width,
    height: 50,
    position: 'absolute',
    left: 0,
    top: 85,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row'
  },

  addressbar_wrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: 0.5,
    shadowOffset: {
      x: 5,
      y: 5
    },
    borderRadius: 10
  },

  addressbar_text: {
    color: 'black',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 20,
    textAlign: 'center'
  },

  modal_container: {
    backgroundColor: 'transparent', 
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center',
    borderRadius: 10
  },
  modal_wrapper: {
    width: 300, 
    height: 300, 
    backgroundColor: 'white'
  },

  modal_image: {
    width: 300, 
    height: 150, 
    resizeMode: 'cover'
  },
  modal_title1: {
    textAlign: 'center', 
    fontSize: 18, 
    color: 'black', 
    marginTop: 10, 
    marginBottom: 10
  },
  modal_title2: {
    textAlign: 'center', 
    fontSize: 15, 
    color: 'grey', 
    marginBottom: 20
  },

  modal_btn_container: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    alignItems: 'center'
  },

  modal_btn1: {
    width: 130, 
    height: 50, 
    backgroundColor: 'black', 
    borderRadius: 25, 
    alignItems: 'center', 
    justifyContent: 'center'
  },

  modal_btn_text: {
    color: 'white',
    fontSize: 17
  },

  statusbar_container: {
    position: 'absolute',
    left: 0,
    bottom: 150,
    width: width,
    justifyContent: 'flex-start',
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },

  statusbar_red: {
    width: 60,
    height: 60,
    backgroundColor: 'red',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  statusbar_green: {
    width: 60,
    height: 60,
    backgroundColor: 'green',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },

  statusbar_yellow: {
    width: 60,
    height: 60,
    backgroundColor: 'yellow',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },

  statusbar_blue: {
    width: 60,
    height: 60,
    backgroundColor: 'blue',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },

  bike_white: {
    width: 40,
    height: 40
  },

  statusbar_label_container: {
     marginLeft: 10,
     width: width - 110,       
     backgroundColor: 'white',
     borderRadius: 25,
     height: 50,
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center'
  },

  statusbar_text: {
    color: 'black',    
    fontSize: 12    
  },

  header_container: {
    backgroundColor: 'white',
    height: 55,
    width: width,
    zIndex: 10000,
    position: 'absolute',
    left: 0,
    top: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,

  },

  back_btn: {
    flexDirection: 'row',
    alignItems: 'center'
  },

  back_btn_img: {
    width: 30, 
    height: 30
  },

  back_btn_text: {
    marginLeft: 5,
    fontSize: 20,
    color: 'rgb(3, 242, 254)',
    fontWeight: 'bold'
  },

  top_btns_container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  top_btn: {
    marginRight: 10
  },

  top_btn_image: {
    width: 30,
    height: 30
  },
  fullscreen_btn: {
    width: 50,
    height: 50,
    backgroundColor: 'black',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    position: 'absolute',
    right: 10,
    top: 140,
    zIndex: 10000
  },

  zoomout_btn: {
    width: 50,
    height: 50,
    backgroundColor: 'black',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    position: 'absolute',
    right: 10,
    top: 200,
    zIndex: 10000,
  },

  zoomin_btn: {
    width: 50,
    height: 50,
    backgroundColor: 'black',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    position: 'absolute',
    right: 10,
    top: 260
  }
});
