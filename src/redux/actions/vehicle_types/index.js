import axios from 'axios';
import urlObj from '../../url';
export const vehicle_types = () => {
  return async (dispatch, getState) => {

    url = urlObj.vehicle_types_url;
    await axios
      .get(url)
      .then((response) => {
        // console.log(response.data)
        dispatch({
          type: 'vehicle_types',
          payload: response.data.vehicle_types

        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};