import create_id from "./create_id"
import delete_id from "./delete_id"
import complaint from "./complaint"
import { combineReducers } from "redux"
import distance_report from './distance_report'
import login from './login'
import vehicle_list from './vehicle_list'
import profile from './profile'
import saveprofile from './saveprofile'
import engine_on from './engine_on'
import engine_off from './engine_off'
import parking_status from './parking_status'
import setSpeed from './setSpeed'
import sub_user from './sub_user'
import all_activity from './all_activity'
import historyreplay from './historyreplay'
import trip_report from './trip_report'
import geofencing from './geofencing'
import savegeofencing from './savegeofencing'
import changepassword from './change_password'
import adddriver from './add_driver'
import registeration from './registeration'
import vehicle_types from './vehicle_types'
import rsalist from './rsalist'
const rootReducer = combineReducers({
  distance_report: distance_report,
  login: login,
  vehicle_list: vehicle_list,
  profile: profile,
  engine_on: engine_on,
  parking_status: parking_status,
  setSpeed: setSpeed,
  historyreplay: historyreplay,
  trip_report: trip_report,
  sub_user: sub_user,
  all_activity: all_activity,
  complaint: complaint,
  delete_id: delete_id,
  create_id: create_id,
  geofencing: geofencing,
  savegeofencing: savegeofencing,
  saveprofile: saveprofile,
  changepassword: changepassword,
  adddriver: adddriver,
  registeration: registeration,
  vehicle_types: vehicle_types,
  rsalist: rsalist,
  engine_off: engine_off

})

export default rootReducer
