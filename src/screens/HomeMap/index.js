import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Dimensions,
  Image,
  ScrollView,
  SafeAreaView,
  Alert
} from "react-native";
const { width, height } = Dimensions.get('window');
import styles from './style';
import MapView, { Marker, Polyline, PROVIDER_GOOGLE } from "react-native-maps";
import KeepAwake from 'react-native-keep-awake';
// import { playButtonPress } from './audio';
import Sound from 'react-native-sound';
import { Images } from '../../utils';
import call from 'react-native-phone-call';
import Modal from 'react-native-modal';
import SendSMS from 'react-native-sms';
import Share from 'react-native-share';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { engine_on } from '../../redux/actions/engine_on';
import { engine_off } from '../../redux/actions/engine_off';
import { parking_status } from '../../redux/actions/parking_status';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faMotorcycle, faCar, faTruck, faTruckPlow, faBus, faTractor } from '@fortawesome/free-solid-svg-icons'
const LATITUDE_DELTA = 0.0009;
const LONGITUDE_DELTA = 0.0009;
Sound.setCategory('Ambient', true);
const engineOnPress = new Sound(require('../../assets/music/newEngine.mpeg'), error => console.log(error));

Sound.setCategory('Ambient', true);
const parkingOnPress = new Sound(require('../../assets/music/newEngine.mpeg'), error => console.log(error));
class HomeMap extends React.Component {
  constructor(props) {
    super(props);
    let initial_date = new Date;
    this.state = {
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
      latitude: 0,
      token: '',
      longitude: 0,
      error: null,
      origin: null,
      vehicle: null,
      height: height * 0.75,
      user_id: this.props.route.params.user_id,
      points: [],
      timer: null,
      full_state: true,
      is_visible_modal: false,
      modal_data_type: 'engine',
      address: 1,
      time: new Date(initial_date.getTime()),
      zoomtime: new Date(initial_date.getTime()),
      zoomtime1: new Date(initial_date.getTime()),
      bottom: 50,
      alarmSound: true,
      refreshRate: 5000,
      vehicle_list: []
    };
    this._getStorageValue()
    this.getUserSetting()
  }
  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value });
      this.setState({ vehicle_id: this.props.route.params.vehicle_id });
    }
    return value
  }

  async getUserSetting() {
    var alarm = await AsyncStorage.getItem('alarmSound')
    var refresh = await AsyncStorage.getItem('refreshRate')
    this.setState({ alarmSound: true, refreshRate: 5000 })
    if (alarm === 'off') {
      this.setState({ alarmSound: false })
    }
    if (refresh > 0) {
      this.setState({ refreshRate: refresh * 5000 })
    }
  }


  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  componentDidMount() {
    this.timerID = setInterval(() => {
      var address = 1;
      var endDate = new Date();
      var seconds = (endDate.getTime() - this.state.time.getTime()) / 1000;
      if (seconds < 20) {
        address = 2
      }
      fetch('http://fasttrackgpsinfo.com/api/show_location3?address=' + this.state.address + '&user_id=1490&vehicle_id=' + this.props.route.params.vehicle_id + '&address=' + address, {
        method: 'GET'
      })
        .then(res => res.json())
        .then(data => {
          if (data.status == 200 && data.location.length > 0) {
            if (this.state.origin == null) {
              this.setState({
                origin: data.location[0]
              });
            }
            this.setState({
              vehicle: data.location[0]
            });
            this.setPoint(data.location[0]);
            this.calculateMapRegion();
          } else {
            this.setState({
              vehicle: null
            });
          }
        })
        .catch(err => {
        });
    }, this.state.refreshRate);
  }
  setPoint(item) {
    var lat = Number(item.latitude);
    var lng = Number(item.longitude);
    var isExist = false;
    for (var i = 0; i < this.state.points.length; i++) {
      if (this.state.points[i].latitude == lat && this.state.points[i].longitude == lng) {
        isExist = true;
        break;
      }
    }
    if (isExist == false) {
      this.setState({
        points: [...this.state.points, {
          latitude: lat,
          longitude: lng,
          heading: 350
        }]
      });
      this.doAnimattion({
        latitude: lat,
        longitude: lng,
        heading: 350
      })
    }
  }
  onCall() {
    if (!this.state.vehicle) {
      return;
    }
    const args = {
      number: this.state.vehicle.driver_no,
      prompt: false
    }
    call(args).catch(console.error)
  }
  playEngineOnPress = () => {
    if (this.state.alarmSound) {
      engineOnPress.play((success) => engineOnPress.reset());
      engineOnPress.setVolume(0.3);
    }
  }
  playParkingOnPress = () => {
    if (this.state.alarmSound) {
      parkingOnPress.play((success) => parkingOnPress.reset());
    }
  }
  onModalButton1(action_type) {
    if (action_type == 'engine') {
      this.props.engine_on(this.state).then(() => {
        console.log(this.props.engine_on1, 'shashanks')
        alert(this.props.engine_on1)
      })
      this.playEngineOnPress()
    }
    if (action_type == 'parking')
      this.props.parking_status(this.state).then(() => {
        alert(this.props.parking_status1)
      })
    this.playParkingOnPress()
    this.setState({
      is_visible_modal: false
    });
  }
  onModalButton2(action_type) {
    if (action_type == 'engine') {
      console.log('api call')
      this.props.engine_off(this.state).then(() => {
        console.log(this.props.engine_off1, 'shashanks')
        alert(this.props.engine_off1)
      })
      this.playEngineOnPress()
    }
    if (action_type == 'parking')
      this.props.parking_status(this.state).then(() => {
        alert(this.props.parking_status1)
      })
    this.playParkingOnPress()
    this.setState({
      is_visible_modal: false
    });
  }
  onShare() {
    if (!this.state.vehicle) {
      return;
    }
    Share.open({
      message: 'http://maps.google.com/maps?q=loc:' + this.state.vehicle.latitude + "," + this.state.vehicle.longitude
    })
      .then((res) => { console.log(res) })
      .catch((err) => { err && console.log(err); });
  }
  onSMS() {
    if (!this.state.vehicle) {
      return;
    }
    Alert.alert('Send SMS', 'Are you sure, You want send sms', [
      {
        text: 'No',
        onPress: () => {
        }
      },
      {
        text: 'Yes',
        onPress: () => {
          SendSMS.send({
            body: 'The default body of the SMS!',
            recipients: [this.state.vehicle.sos_no],
            successTypes: ['sent', 'queued'],
          }, (completed, cancelled, error) => {
            console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + 'error: ' + error);
          });
        }
      }
    ]);
  }

  onMarkerPress = () => {
    this.setState({ time: new Date(), zoomtime: new Date() })
    if (this.state.height === height * 0.75) {
      this.setState({
        height: height * 0.74
      })
    }
    console.log('vehicle_no', 'clicked')
  }
  showMarker(item, isOrigin) {
    if (item == null) {
      return;
    }
    var coordinate = {
      longitude: Number(item.longitude) ? Number(item.longitude) : 0,
      latitude: Number(item.latitude) ? Number(item.latitude) : 0,
    };
    if (isOrigin == true) {
      return (
        <Marker.Animated
          coordinate={coordinate}
          onPress={(e) => { e.stopPropagation(); this.onMarkerPress() }}
          anchor={{ x: 0.5, y: 0.5 }}
          style={{
            width: 16, height: 18, transform: [{
              rotate: item.heading ? item.heading : '0deg'
            }]
          }}
        />
      );
    } else {
      return (
        <Marker.Animated
          coordinate={coordinate}
          anchor={{ x: 0.5, y: 0.5 }}
          onPress={(e) => { e.stopPropagation(); this.onMarkerPress() }}
          image={item.color == 'green' && item.vehicle_type == 'Bike' ? Images.bike_green : item.color == 'red' && item.vehicle_type == 'Bike' ? Images.bike_red : item.color == 'blue' && item.vehicle_type == 'Bike' ? Images.bike_blue : item.color == 'yellow' && item.vehicle_type == 'Bike' ? Images.bike_yellow : item.color == 'green' && item.vehicle_type == 'Motor Car' ? Images.car_green : item.color == 'red' && item.vehicle_type == 'Motor Car' ? Images.car_red : item.color == 'yellow' && item.vehicle_type == 'Motor Car' ? Images.car_yellow : item.color == 'blue' && item.vehicle_type == 'Motor Car' ? Images.car_blue : item.color == 'green' && item.vehicle_type == 'Bus' ? Images.bus_green : item.color == 'yellow' && item.vehicle_type == 'Bus' ? Images.bus_yellow : item.color == 'red' && item.vehicle_type == 'Bus' ? Images.bus_red : Images.car_red}
          style={{
            width: 16, height: 18, transform: [{
              rotate: item.heading ? item.heading : '0deg'
            }]
          }}
          resizeMode="contain"
        />
      );
    }
  }

  showLine() {
    return (
      <Polyline
        coordinates={this.state.points}
        strokeColor="black"
        strokeColors={[
          '#7F0000',
          '#B24112',
          '#E5845C',
          '#238C23',
          '#7F0000'
        ]}
        strokeWidth={3}
      />
    );
  }
  calculateMapRegion() {
    return this.getRegionForCoordinates(this.state.points)
  }
  getRegionForCoordinates(points) {
    // points should be an array of { latitude: X, longitude: Y }
    let minX, maxX, minY, maxY;
    // init first point
    ((point) => {
      minX = point.latitude;
      maxX = point.latitude;
      minY = point.longitude;
      maxY = point.longitude;
    })(points[0]);
    // calculate rect
    points.map((point) => {
      minX = Math.min(minX, point.latitude);
      maxX = Math.max(maxX, point.latitude);
      minY = Math.min(minY, point.longitude);
      maxY = Math.max(maxY, point.longitude);
    });
    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;
    const deltaX = (maxX - minX);
    const deltaY = (maxY - minY);
    this.setState({
      latitudeDelta: deltaX,
      longitudeDelta: deltaY,
      longitude: midY,
      latitude: midX
    });
    return {
      latitude: midX,
      longitude: midY,
      latitudeDelta: deltaX,
      longitudeDelta: deltaY
    };
  }
  onRegionChange() {
    this.state.zoomtime = this.state.zoomtime1
    this.state.zoomtime1 = new Date()


    console.log('apne aap ho rha hai')
  }
  render() {
    var mapStyle = [
      { "featureType": "road", "elementType": "geometry", "stylers": [{ "color": "#F9E9D0" }] },
    ];
    return (
      <SafeAreaView style={styles.container}>
        {/*header bar: begin*/}
        <SafeAreaView style={styles.header_container}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <View style={styles.back_btn}>
              <Image source={Images.icon_back_blue} style={styles.back_btn_img} />
              <Text style={styles.back_btn_text}>{this.state.vehicle ? this.state.vehicle.vehicle_no : ''}</Text>
            </View>
          </TouchableOpacity>
          <View style={styles.top_btns_container}>
            <TouchableOpacity style={styles.top_btn} onPress={() => this.onSignal()}>
              <Image source={Images.icon_signal_blue} style={styles.top_btn_image} />
            </TouchableOpacity>
            <TouchableOpacity style={styles.top_btn} onPress={() => this.onRefrsh()}>
              <Image source={Images.refresh_blue} style={styles.top_btn_image} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.onNotification()}>
              <Image source={Images.icon_notification_blue} style={styles.top_btn_image} />
            </TouchableOpacity>
          </View>
        </SafeAreaView>
        {/*header bar: end*/}
        <MapView
          region={{
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
            longitude: this.state.longitude,
            latitude: this.state.latitude
          }}
          // onRegionChange={() => this.onRegionChange()}
          provider={PROVIDER_GOOGLE}
          showsMyLocationButton={true}
          showsCompass={true}
          showsTraffic={true}
          provider={null}
          toolbarEnabled={true}
          paddingAdjustmentBehavior={'always'}
          customMapStyle={mapStyle}
          mapPadding={{
            top: 50,
            right: 50,
            bottom: this.state.bottom,
            left: 50
          }}
          style={[styles.maparea, { height: this.state.height }]}
          ref={ref => this.map = ref}
        >
          {this.showLine()}
          {this.showMarker(this.state.origin, true)}
          {this.showMarker(this.state.vehicle, false)}
        </MapView>

        <View style={styles.statusbar_container}>
          {this.state.vehicle ?
            // <View style={this.state.vehicle.color == 'red' ? styles.statusbar_red : this.state.vehicle.color == 'green' ? styles.statusbar_green : this.state.vehicle.color == 'yellow' ? styles.statusbar_yellow : styles.statusbar_blue}>
            //   <Image source={!this.state.vehicle ? Images.bike_white : this.state.vehicle.vehicle_type == 'Bike' ? Images.bike_white : Images.car_white} style={styles.bike_white} />
            // </View> 
            <View style={this.state.vehicle.color == 'red' ? styles.statusbar_red : this.state.vehicle.color == 'green' ? styles.statusbar_green : this.state.vehicle.color == 'yellow' ? styles.statusbar_yellow : styles.statusbar_blue}>
              <FontAwesomeIcon icon={this.state.vehicle.vehicle_type == 'Bike' ? faMotorcycle : this.state.vehicle.vehicle_type == 'Motor Car' ? faCar : this.state.vehicle.vehicle_type == 'Truck' ? faTruck : this.state.vehicle.vehicle_type == 'Tractor' ? faTractor : faBus} size={35} color={'white'} />
            </View>
            : <></>
          }
          <View style={styles.statusbar_label_container}>
            <Text style={styles.statusbar_text}>
              {this.state.vehicle ? this.state.vehicle.vehicle_no + ' ' + this.state.vehicle.position : 'Location of car status data are not found'}
            </Text>
          </View>
        </View>
        <View style={styles.bottom_tab_container}>
          <SafeAreaView style={{ flex: 1 }}>
            <ScrollView
              bounces={true}
              horizontal={true}
              bounces={true}
              disableIntervalMomentum={true}
              showsHorizontalScrollIndicator={true}
              scrollEventThrottle={200}
              decelerationRate="fast"
            >
              <TouchableOpacity style={styles.bottom_btn} onPress={() => this.onCall()}>
                <View style={styles.bottom_btn_image_wrapper}>
                  <Image source={Images.phone_contact_b} style={styles.image_button} />
                </View>
                <Text style={styles.bottom_btn_text1}>Call Driver</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottom_btn} onPress={() => this.props.navigation.navigate('AllActivity', + { vehicle_id: this.props.route.params.vehicle_id })}>
                <View style={styles.bottom_btn_image_wrapper}>
                  <Image source={Images.list_b} style={styles.image_button} />
                </View>
                <Text style={styles.bottom_btn_text1}>All Activity</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottom_btn} onPress={() => { this.setState({ is_visible_modal: true, modal_data_type: 'engine' }) }}>
                <View style={!this.state.vehicle ? styles.bottom_btn_image_wrapper : (this.state.vehicle.engine_status == 0) ? styles.bottom_btn_image_wrapper : styles.bottom_btn_image_wrapper_stop}>
                  <Image source={Images.engine_b} style={styles.image_button} />
                </View>
                <Text style={styles.bottom_btn_text1}>Engine</Text>
                <Text style={styles.bottom_btn_text2}>
                  {!this.state.vehicle ? '(On/Off)' : (this.state.vehicle.engine_status == 1 ? 'Off' : 'On')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottom_btn} onPress={() => { this.setState({ is_visible_modal: true, modal_data_type: 'parking' }) }}>
                <View style={!this.state.vehicle ? styles.bottom_btn_image_wrapper : (this.state.vehicle.parking_status == 0) ? styles.bottom_btn_image_wrapper : styles.bottom_btn_image_wrapper_stop}>
                  <Image source={Images.car_parked_b} style={styles.image_button} />
                </View>
                <Text style={styles.bottom_btn_text1}>Car Parking</Text>
                <Text style={styles.bottom_btn_text2}>
                  {!this.state.vehicle ? '(On/Off)' : (this.state.vehicle.parking_status == 0 ? 'Off' : 'On')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottom_btn} onPress={() => this.onShare()}>
                <View style={styles.bottom_btn_image_wrapper}>
                  <Image source={Images.location_g_new} style={styles.image_button} />
                </View>
                <Text style={styles.bottom_btn_text1}>Share</Text>
                <Text style={styles.bottom_btn_text2}>Location</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bottom_btn} onPress={() => this.onSMS()}>
                <View style={styles.bottom_btn_image_wrapper}>
                  <Image source={Images.sos} style={styles.image_button} />
                </View>
                <Text style={styles.bottom_btn_text1}>SOS</Text>
              </TouchableOpacity>
            </ScrollView>
          </SafeAreaView>
        </View>
        {(this.state.vehicle) &&
          (
            <View style={styles.speed_text_wrapper}>
              <Text style={styles.speed_text}>{this.state.vehicle.speed}</Text>
              <Text style={styles.speed_label}> km/h</Text>
            </View>
          )}
        {(this.state.vehicle) &&
          (
            <View style={styles.addressbar_container}>
              <View style={styles.addressbar_wrapper}>
                <Image source={Images.pin_location} style={{ width: 5, height: 25, marginRight: 20 }} />
                <Text style={styles.addressbar_textss}>
                  {this.state.vehicle.address}
                </Text>
              </View>
            </View>
          )}
        <Modal isVisible={this.state.is_visible_modal}>
          <TouchableOpacity
            style={styles.container}
            activeOpacity={1}
            onPressOut={() => { this.setState({ is_visible_modal: false }) }}
          >
            <ScrollView
              directionalLockEnabled={true}
              contentContainerStyle={styles.scrollModal}
            >
              <TouchableWithoutFeedback>
                <View style={styles.modal_container}>
                  <View style={styles.modal_wrapper}>
                    <Image source={this.state.modal_data_type == 'engine' ? Images.engine_b : Images.car_parked_b} style={styles.modal_image} />
                    <Text style={styles.modal_title1}>
                      {this.state.modal_data_type == 'engine' ? 'Engine status' : 'Parking the car'}
                    </Text>
                    <Text style={styles.modal_title2}>
                      {this.state.modal_data_type == 'engine' ? 'Do you want change Engine Status?' : 'Do you want no parking the car?'}
                    </Text>

                    <View style={styles.modal_btn_container}>
                      <TouchableOpacity style={styles.modal_btn1} onPress={() => this.onModalButton1(this.state.modal_data_type)}>
                        <Text style={styles.modal_btn_text}>
                          {this.state.modal_data_type == 'engine' ? 'Engine On' : 'Parking On'}
                        </Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.modal_btn1} onPress={() => this.onModalButton2(this.state.modal_data_type)}>
                        <Text style={styles.modal_btn_text}>
                          {this.state.modal_data_type == 'engine' ? 'Engine Off' : 'Parking Off'}
                        </Text>
                      </TouchableOpacity>
                    </View>

                  </View>
                </View>
              </TouchableWithoutFeedback>
            </ScrollView>
          </TouchableOpacity>
        </Modal>
        <KeepAwake />
      </SafeAreaView  >
    );
  }
}
const mapStateToProps = (state) => {
  return {
    engine_on1: state.engine_on.engine_on,
    engine_off1: state.engine_off.engine_off,
    parking_status1: state.parking_status.parking_status,
  };
};
export default connect(mapStateToProps, {
  engine_on, parking_status, engine_off
})(HomeMap);
