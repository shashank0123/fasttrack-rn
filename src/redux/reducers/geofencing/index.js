const initialState = {
    geofencing: {}
  };
  
  export const geofencing = (state = initialState, action) => {
    switch (action.type) {
      case 'GEOFENCING': {
        // console.log(action.payload)
        return { ...state,geofencing:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default geofencing;