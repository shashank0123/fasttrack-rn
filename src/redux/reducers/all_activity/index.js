const initialState = {
    all_activity: []
  };
  
  export const all_activity = (state = initialState, action) => {
    switch (action.type) {
      case 'all_activity': {
        // console.log(action.payload)
        return { ...state,all_activity:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default all_activity;