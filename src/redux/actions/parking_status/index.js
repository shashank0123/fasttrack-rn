import axios from 'axios';
import urlObj from '../../url';
export const parking_status = (data) => {
  return async (dispatch, getState) => {
    
    url = urlObj.parking_status_url+"?token="+data.token+"&vehicle_id="+data.vehicle_id;
    console.log(url)
    await axios
      .post(url)
      .then((response) => {
        console.log(response)
        dispatch({
          type: 'LOGIN',
          payload: response.data.data
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};