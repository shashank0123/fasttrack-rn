import React from 'react';
import { ActivityIndicator } from 'react-native';


const LoadingIcon = ({ loadingApp }) => <ActivityIndicator 
style={{zIndex:5, position:'absolute',alignItems:'center', top:'55%', left:'45%', }} size="large" color="#00b8e6" animating={loadingApp} ></ActivityIndicator>;

export default LoadingIcon;