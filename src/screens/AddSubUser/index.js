import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Dimensions,
  Image,
  FlatList,
  Button, SafeAreaView
} from "react-native";

import styles from './style';
import { Images } from '../../utils';
// import {Picker} from '@react-native-picker/picker';
import { Picker } from 'react-native-woodpicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MultiSelect from 'react-native-multiple-select';
import { connect } from 'react-redux';
import { sub_user } from '../../redux/actions/sub_user';
import { vehicle_list } from '../../redux/actions/vehicle_list';
import { create_id } from '../../redux/actions/create_id';

const items = [{
  id: '1',
  name: 'kittu'
}, {
  id: '2',
  name: 'HR0394D'
}, {
  id: '3',
  name: 'DLWSKL0987'
},
];
class AddSubUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hidePassword: true,
      selectedValue: '',
      pickedVehicle: '',
      vehicle_list: [],
      selectedItems: [],
      sub_user: []
    }
    this._getStorageValue()
  }

  submitUser() {
    this.props.create_id(this.state).then(() => {
      this.props.navigation.replace('CreateId')
      console.log(this.props.create_id1)
    })
  }

  async _getStorageValue() {
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      console.log(value)
      this.setState({ token: value })
      this.props.vehicle_list(value).then(() => {



        var data = this.props.vehicle_list1.map((element) => {
          return { id: element.vehicle_id, name: element.vehicle_no }
        })
        this.setState({ vehicle_list: data })
        console.log(this.state.vehicle_list)
      })


    }
    return value
  }

  onSelectedItemsChange = selectedItems => {
    this.setState({ selectedItems });
    this.setState({ vehicle_id: selectedItems.join() })
    console.log(this.state.vehicle_id)
  };



  render() {
    const { selectedItems } = this.state;
    // console.log(this.props.vehicle_list1)
    // console.log(this.props.sub_user1)
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.upperBar}>
            <View style={styles.backIconContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Images.back} style={styles.barMenuIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.headingContainer}>
              <Text style={styles.barText}>Add Sub User</Text>
            </View>
          </View>
          <KeyboardAvoidingView behavior="padding">
            <ScrollView
              contentContainerStyle={styles.scrollWrapper}
              showsVerticalScrollIndicator={false}>
              <View style={styles.formWrapper}>


                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    onChangeText={(name) => { this.setState({ name }) }}
                    autoCapitalize="none"
                    placeholder="Name" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    onChangeText={(user_name) => { this.setState({ user_name, email: user_name }) }}
                    autoCapitalize="none"
                    placeholder="User Name" />
                  <Image source={Images.person_Icon} style={styles.searchIcon} />
                </View>
                {/* <View style={styles.formBox}>
              <TextInput style={styles.formshortInput}
                placeholderTextColor="#6f7a8c"
                onChangeText={(password) => { this.setState({ password }) }}
                placeholder="Password" />
              <Image source={Images.eye} style={styles.searchIcon} />
            </View> */}
                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    onChangeText={password => this.setState({ password })}
                    placeholder="Password"
                    autoCapitalize="none"
                    secureTextEntry={this.state.hidePassword} />
                  <TouchableOpacity onPress={() => { this.setState({ hidePassword: !(this.state.hidePassword) }) }}>
                    <Image source={(this.state.hidePassword) ? (Images.eye_line) : (Images.eye)} style={styles.searchIcon} />
                  </TouchableOpacity>
                </View>



                <View style={styles.formBox}>
                  <TextInput style={styles.formshortInput}
                    placeholderTextColor="#6f7a8c"
                    onChangeText={(mobile) => { this.setState({ mobile }) }}
                    keyboardType='numeric'
                    maxLength={10}
                    placeholder="Mobile Number" />
                  <Image source={Images.call_Icon} style={styles.callIcon} />
                </View>
                {/* <View style={styles.formBox}> */}
                <MultiSelect

                  styleDropdownMenu={{ width: 355, marginTop: 10, borderRadius: 10, }}
                  styleItemsContainer={{ width: 356, marginTop: '5%' }}
                  styleMainWrapper={{ backgroundColor: '#fff', width: 360, marginTop: '8%', borderWidth: 2, }}
                  hideTags
                  items={this.state.vehicle_list}
                  uniqueKey="id"
                  ref={(component) => { this.multiSelect = component }}
                  onSelectedItemsChange={this.onSelectedItemsChange}
                  selectedItems={selectedItems}
                  selectText=" Click Here to Select Vehicle"
                  searchInputPlaceholderText="Select Vehicle"
                  onChangeInput={(text) => console.log(text)}
                  tagRemoveIconColor="#000"
                  tagBorderColor="#CCC"
                  tagTextColor="#000"
                  selectedItemTextColor="#000"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  displayKey="name"
                  searchInputStyle={{ color: '#000' }}
                  submitButtonColor="#000"
                  submitButtonText="Submit"
                />
                {/* </View> */}
                <View style={{ marginTop: 5, justifyContent: 'center', alignItems: 'center', marginLeft: 30 }}>
                  {/* {this.multiSelect.getSelectedItemsExt(selectedItems)} */}
                  {this.multiSelect && this.multiSelect.getSelectedItemsExt(selectedItems)}
                </View>
                {/* </View> */}

                <TouchableOpacity style={styles.submit} onPress={() => { this.submitUser() }}>
                  <Text style={styles.submitText}>Submit</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    sub_user1: state.sub_user.sub_user,
    create_id1: state.create_id.create_id,
    vehicle_list1: state.vehicle_list.vehicle_list,
  };
};
export default connect(mapStateToProps, {
  sub_user, create_id, vehicle_list
})(AddSubUser);

