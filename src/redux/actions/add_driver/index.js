import axios from 'axios';
import urlObj from '../../url';
export const adddriver = (state) => {
  return async (dispatch, getState) => {
    
    url = urlObj.add_driver_url+"?token="+state.token+ "&driver_name=" + state.driver_name + "&driver_phone=" + state.driver_phone  + "&driver_email=" + state.driver_email  + "&driver_gender=" + state.driver_gender  + "&driver_state=" + state.driver_state + "&driver_city=" + state.driver_city  + "&driver_phone=" + state.driver_phone  + "&driver_dob=" + state.driver_dob  + "&driver_country=" + state.driver_country  + "&driver_pincode=" + state.driver_pincode  + "&driver_pan_no=" + state.driver_pan_no  + "&driver_drive_licence_no=" + state.driver_drive_licence_no;
    
    console.log(url)
    await axios
      .post(url)
      .then((response) => {
        dispatch({
          type: 'LOGIN',
          payload: response.data
          
        });
        // console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
  };
};