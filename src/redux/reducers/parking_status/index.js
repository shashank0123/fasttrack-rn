const initialState = {
    parking_status: {}
  };
  
  export const parking_status = (state = initialState, action) => {
    switch (action.type) {
      case 'LOGIN': {
        // console.log(action.payload)
        return { ...state,parking_status:action.payload };
      }
      default: {
        return state;
      }
    }
  };
  export default parking_status;