import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
  FlatList, SafeAreaView
} from "react-native";
const { width, height } = Dimensions.get('window');
import styles from './style';
import { Images } from '../../utils';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { vehicle_list } from '../../redux/actions/vehicle_list';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faMotorcycle, faCar, faTruck, faTruckPlow, faBus, faTractor } from '@fortawesome/free-solid-svg-icons'
import AppIntroSlider from 'react-native-app-intro-slider';
const introSlider = [
  {
    text: 'text 1',
    image: Images.p1,
    bg: '#59b2ab',
  },
  {
    text:
      'text 2',
    image: Images.p2,
    bg: '#febe29',
  },
  {
    text:
      'text 3',
    image: Images.p3,
    bg: '#22bcb5',
  },
  {
    text:
      'text 4',
    image: Images.p4,
    bg: '#22bcb5',
  },
];
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: true,
    }
    this._getStorageValue()
  }
  state = { vehicle_list: [] }
  async _getStorageValue() {
    var fcmToken = await AsyncStorage.getItem('fcmToken')
    var value = await AsyncStorage.getItem('token')
    if (value != null) {
      this.props.vehicle_list(value).then(async () => {
        this.setState({
          data: this.props.vehicle_list1,
          loading: false,
          viewAnimation: true,
        });
        // console.log(this.props.vehicle_list1.vehicle_id, "yaha h vehicle id")
        // if (this.props.vehicle_list1.length  == 0){
        //   await AsyncStorage.removeItem('token')
        //   this.props.navigation.navigate('Login')
        // }
        this.arrayholder = this.state.vehicle_list1;
      })
      this.setState({ token: value })
    }
    return value
  }
  componentDidMount() {
    this.timerID = setInterval(() => {
      this.props.vehicle_list(this.state.token).then(() => {
        this.setState({
          data: this.props.vehicle_list1,
          loading: false,
          viewAnimation: true,
        });
        if (this.state.data.length == 0) {
          clearInterval(this.timerID);
          this.logout()
        }
      })
        .catch(err => {
          clearInterval(this.timerID);
          this.logout()
        });
    }, 10000);
  }
  _renderItem = ({ item }) => {
    return (
      <SafeAreaView style={styles.slideCont}>
        <Image
          source={item.image}
          style={styles.img}
          resizeMode={'stretch'}
        />
        <View style={styles.textCont2}>
          <Text style={styles.text}>{item.text}</Text>
        </View>
      </SafeAreaView>
    );
  }
  _onDone = () => {
    this.setState({ showRealApp: true });
  };
  _onSkip = () => {
    this.setState({ showRealApp: true });
  };
  _renderNextButton = () => {
    return (
      <View>
        <Image source={Images.bannerBtn} style={{ marginRight: width * 0.02 }} />
      </View>
    );
  };
  _renderSkipButton = () => {
    return (
      <View style={styles.skipBox}>
        <Text style={styles.skipText}>Skip</Text>
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <View>
        <Image source={Images.bannerBtn} style={{ marginRight: width * 0.02 }} />
      </View>
    );
  };
  async logout() {
    await AsyncStorage.removeItem('token')
    await AsyncStorage.removeItem('fcmToken')
    this.props.navigation.replace('Login')
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  searchFilterFunction = text => {
    this.setState({
      value: text,
    });
    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.name.toUpperCase()}`;
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };
  render() {
    if (this.state.showRealApp) {
      return (
        <>
          <SafeAreaView style={styles.container}>
            <View style={styles.wrapper}>
              <View style={styles.upperBar}>
                <TouchableOpacity onPress={() => { this.props.navigation.openDrawer(); }}>
                  <Image source={Images.navigation_Menu} style={styles.barMenuIcon} />
                </TouchableOpacity>
                <Text style={styles.barText}>Vehicle List</Text>
                <TouchableOpacity style={styles.barNavIcon} onPress={() => this.props.navigation.navigate('AllVehicleMap', { vehicle_id: this.state.data.vehicle_id, vehicle_name: this.state.data.vehicle_name, user_id: this.state.data.user_id })}>
                  <Image source={Images.marker_navigation_blue} style={styles.barNavIcon1} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.barRefreshIcon} onPress={() => { this.props.navigation.replace('Home'); }}>
                  <Image source={Images.refresh_blue} style={styles.barRefreshIcon1} />
                </TouchableOpacity>
              </View>
              <View style={styles.searchWrapper}>
                <Image source={Images.searchMenu_icon} style={styles.searchMenuIcon} />
                <TextInput style={styles.textInputStyle}
                  placeholder="Search Vehicle..."
                  placeholderTextColor='#6f7a8c'
                  underlineColorAndroid="transparent"
                  textAlignVertical='center'
                  value={this.state.name}
                  onChangeText={text => this.searchFilterFunction(text)}
                />
                <Image source={Images.search_grey} style={styles.searchIcon} />
              </View>
              <View style={styles.flatlistWrapper}>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  keyExtractor={(item, index) => index.toString()}
                  data={this.state.data}
                  renderItem={({ item }) =>
                    // <Card style={styles.card}>
                    <View style={styles.card}>
                      <View style={styles.cardHeader}>
                        <View style={styles.nameWrapper}>
                          <Text numberOfLines={1} style={styles.name}>{item.vehicle_no}</Text>
                        </View>
                        <View style={styles.slWrapper}>
                          <Text numberOfLines={1} style={styles.speed_limit}>Speed Limit: {item.speed_limit}(KM/H)</Text>
                        </View>
                        <View style={styles.dateWrapper}>
                          <Text style={styles.date}>{item.created_at}</Text>
                        </View>
                      </View>
                      <View style={styles.cardImages}>
                        <View style={styles.cardImagesWrapper}>
                          <View style={item.color == 'red' ? styles.cardStatusCircleRed : item.color == 'green' ? styles.cardStatusCircleGreen : item.color == 'yellow' ? styles.cardStatusCircleYellow : styles.cardStatusCircleBlue}>
                            <FontAwesomeIcon icon={item.vehicle_type == 'Bike' ? faMotorcycle : item.vehicle_type == 'Motor Car' ? faCar : item.vehicle_type == 'Truck' ? faTruck : item.vehicle_type == 'Tractor' ? faTractor : faBus} size={35} color={'white'} />
                          </View>
                          <Text style={styles.cardImagesText}>Status</Text>
                          <Text numberOfLines={1} style={styles.cardImagesText}>{item.status}</Text>
                        </View>
                        <View style={styles.cardImagesWrapper}>
                          <View style={styles.cardCircle}>
                            <Image source={Images.speed} style={styles.circleIcons} />
                          </View>
                          <Text style={styles.cardImagesText}>Engine</Text>
                          <Text numberOfLines={1} style={styles.cardImagesText}>{item.engine}</Text>
                        </View>
                        <View style={styles.cardImagesWrapper}>
                          <View style={styles.cardCircle}>
                            <Image source={Images.engine_b} style={styles.circleIcons} />
                          </View>
                          <Text style={styles.cardImagesText}>Speed(KM/H)</Text>
                          <Text numberOfLines={1} style={styles.cardImagesText}>{item.speed}</Text>
                        </View>
                        <TouchableOpacity style={styles.submit} onPress={() => this.props.navigation.navigate('HomeMap', { vehicle_name: item.vehicle_name, vehicle_id: item.vehicle_id, user_id: item.user_id })}>
                          {/* <View style={styles.cardImages}> */}
                          <View style={styles.cardImagesWrapper1}>
                            <View style={styles.cardCircle}>
                              <Image source={Images.marker_navigation_red} style={styles.circleIcons} />
                            </View>
                            <Text style={styles.cardImagesText}>Track</Text>
                          </View>
                          {/* </View> */}
                        </TouchableOpacity>
                      </View>
                      <View style={styles.cardAddress}>
                        <Text numberOfLines={1} style={styles.address}>{item.address}</Text>
                        {/* <Text style={styles.address}>Hello</Text> */}
                      </View>
                    </View>
                  } />
              </View>
            </View>
          </SafeAreaView>
        </>
      );
    } else {
      return (
        <AppIntroSlider
          renderItem={this._renderItem}
          data={introSlider}
          onDone={this._onDone}
          onSkip={this._onSkip}
          dotStyle={{ backgroundColor: '#CBD5D7' }}
          activeDotStyle={{ backgroundColor: '#5588E7' }}
          renderSkipButton={this._renderSkipButton}
          renderNextButton={this._renderNextButton}
          renderDoneButton={this._renderDoneButton}
          showSkipButton={true}
        />
      );
    }
  }
}
const mapStateToProps = (state) => {
  return {
    vehicle_list1: state.vehicle_list.vehicle_list
  };
};
export default connect(mapStateToProps, {
  vehicle_list
})(Home);
