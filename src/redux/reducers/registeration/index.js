const initialState = {
  registeration: {}
};

export const registeration = (state = initialState, action) => {
  switch (action.type) {
    case 'MAINUSER': {
      return { ...state, registeration: action.payload };
    }
    default: {
      return state;
    }
  }
};
export default registeration;